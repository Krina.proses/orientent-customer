import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React from "react";
import styles from "../EarSurgeriesListModule/EarSurgeries.module.scss";
import { NODE_API_URL, routeBaseUrl } from "App/utils/constants";
import DashboardIcon from "App/orienticons/DashboardIcon";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLeaf } from "@fortawesome/free-solid-svg-icons";

const EarListDataArray = [
  {
    id: 1,
    title: "Ear Anatomy",
    link: "EarSurgeries/EarAnatomy",
    class: "",
    svgclr: "skyblue",
    // class:'start'
    // subList: {
    //   headerTitle: "Ear Anatomy",
    //   downloadImageName: "/Ear/outer-ear-with-text.jpg",
    // },
  },
  {
    id: 2,
    title: "Tympanoplasty",
    link: "EarSurgeries/Tympanoplasty",
    class: "",
    svgclr: "bgblue",
    // subList: {
    //   headerTitle: "Surgical repair of the eardrum or tympanic membrance(TM)",
    //   downloadImageName: "Tympanosplasty.jpg",
    // },
  },
  {
    id: 3,
    title: "Stapedectomy",
    link: "EarSurgeries/Stapedectomy",
    class: "",
    // class: "end",
    svgclr: "bgorange",
  },
  {
    id: 4,
    title: "Mastoidectomy",
    link: "EarSurgeries/Mastoidectomy",
    class: "",
    svgclr: "bgpink",
  },
  {
    id: 5,
    title: "Ossiculoplasty",
    link: "EarSurgeries/Ossiculoplasty",
    class: "",
    svgclr: "bgyellow",
  },
  {
    id: 6,
    title: "Cochlear Implant",
    link: "EarSurgeries/CochlearImplant",
    class: "",
    svgclr: "bggreen",
  },
];

function EarListData() {
  return (
    <>
      <Box>
        <Box className={styles.bgCurve}>
        <Box className="container">
        <Box className={`${"row justify-content-center"} ${styles.leafBoxes}`}>
          {EarListDataArray.map((item: any, index: number) => {
            return (
              <Box className="col col-2 mb-3 mt-5 justify-content-center">
                <a
                  href={`${routeBaseUrl}/${item.link}`}
                  className={`${styles.featuresltem} ${styles[item.class]}`}
                >
                  <Box className={`${styles[item.svgclr]}`} position="middle">
                    <Box>
                      <FontAwesomeIcon
                        icon={faLeaf}
                        flip="horizontal"
                        fontSize={50}
                        color="blue"
                      />
                    </Box>
                  </Box>

                  <Text className={styles.title}>{item.title}</Text>
                </a>
              </Box>
            );
          })}
        </Box>
        </Box>
        </Box>
      </Box>
    </>
  );
}

export default EarListData;
