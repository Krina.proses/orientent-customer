import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
        <li>Pain and uncomfortable feeling in the ear for a few days.</li>
        <li>Ear dressing for about 3 weeks, which causes muffled hearing.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export default AfterCare;
