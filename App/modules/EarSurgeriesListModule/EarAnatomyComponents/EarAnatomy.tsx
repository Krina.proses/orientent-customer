import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../EarSurgeries.module.scss";
import EarSurgeryLayout from "../EarSurgeryLayout";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

// const EarListDataArray = [
//   {
//     id: 1,
//     title: "Ear Anatomy",
//     subList: {
//       headerTitle: "Ear Anatomy",
//       downloadImageName: "/Ear/outer-ear-with-text.jpg",
//     },
//   },
//   {
//     id: 2,
//     title: "Tympanoplasty",
//     subList: {
//       headerTitle: "Surgical repair of the eardrum or tympanic membrance(TM)",
//       downloadImageName: "Tympanosplasty.jpg",
//     },
//   },
//   {
//     id: 3,
//     title: "Stapedectomy",
//     subList: {
//       headerTitle: "Surgical removal of stapes",
//       downloadImageName: "stapes.jpg",
//     },
//   },
// ];

function EarAnatomy() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleClickImage = () => {
    router.push(`/EarSurgeries/InnerEar`);
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <EarSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              {/* <Text size="h2" className="mb-0">Ear Anatomy</Text> */}
              <Text className="topHeading">Ear Anatomy</Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                // canvasWidth={1320}
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  color: "red",
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                  // opacity : 0
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    {/* <a href="/EarSurgeries/InnerEar"> */}
                    <div className={styles.imageFrame}>
                      <div
                        className={styles.dotFrame}
                        style={{
                          left: "51%",
                          top: "51%",
                          // position: "relative",
                          //zIndex: draw ? 1 : 2,
                        }}
                      >
                        <div className={styles.dot} onClick={handleClickImage}>
                          <div className={styles.circle}></div>

                          <div className={styles.infoSurgery}></div>
                        </div>
                      </div>

                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Ear/outer-ear-with-text.jpg`}
                      />
                    </div>
                    {/* </a> */}
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>

          {/* <img
                    className="img-fluid"
                    width="100%"
                    src="/Ear/outer-ear-with-text.jpg"
                  /> */}
        </Box>
      </EarSurgeryLayout>
    </>
  );
}

export default EarAnatomy;
