import React from "react";
import EarListData from "./EarListData";
import EarSurgeryLayout from "./EarSurgeryLayout";
import EarSurgeriesBanner from "./EarSurgeriesBanner";


const EarSurgeries = () => {
  return (
    <>
      <EarSurgeryLayout>
      {/* <Box className="row">
          <Box className="col-lg-4 col-md-6 col-sm-6 col-12"> */}
            {/* <Text>Ear Surgeries Module</Text> */}
            {/* <EarAnatomy /> */}
            <EarSurgeriesBanner/>
            <EarListData />
          {/* </Box> */}
          {/* <Box className="col-lg-4 col-md-6 col-sm-6 col-12">
            <Text>Ear Surgeries Module</Text>
            <EarAnatomy />
          </Box> */}
         
        {/* </Box> */}
      </EarSurgeryLayout>
      </>
  );
};

export default EarSurgeries;
