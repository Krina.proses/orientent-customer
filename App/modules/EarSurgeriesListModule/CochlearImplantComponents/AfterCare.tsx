import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
        <li>A special bandage is worn on the head during sleep for a short period post-surgery.</li>
        <li>The external parts of the device are fitted, turned on and mapped after the wound heals (around 1 month after surgery).</li>
        <li>Need for training to interpret sounds heard through the device.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export default AfterCare;
