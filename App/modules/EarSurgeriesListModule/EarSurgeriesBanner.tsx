
import Box from '@ui/Box/Box'
import React from 'react'
import styles from "./EarSurgeries.module.scss";
import Text from '@ui/Text/Text';

const EarSurgeriesBanner = () => {
  return (
   <>
 
   <Box>
   <Box className={`${styles.innerbanner}`}>
    {/* <Box className="container"> */}
    <Text className={styles.bannerTitle}>Ear Surgeries</Text>
   {/* <img
            className="img-fluid"
            width="100%"
            src="/ear.jpg"
            
          /> */}
   </Box>
   </Box>
   {/* </Box> */}
  
   </>
  )
}

export default EarSurgeriesBanner

