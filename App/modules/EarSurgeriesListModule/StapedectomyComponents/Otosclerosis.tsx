import React, { useRef, useState } from "react";
import EarSurgeryLayout from "../EarSurgeryLayout";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import styles from "../EarSurgeries.module.scss";
import Undo from "App/orienticons/Undo";
import Cross from "App/orienticons/Cross";
import Pen from "App/orienticons/Pen";
import CanvasDraw from "react-canvas-draw";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";
import router from "next/router";

function Otosclerosis() {
  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <EarSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">Otosclerosis</Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Stapedectomy/otosclerosis.jpg`}
                      />
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
        </Box>
      </EarSurgeryLayout>
    </>
  );
}

export default Otosclerosis;
