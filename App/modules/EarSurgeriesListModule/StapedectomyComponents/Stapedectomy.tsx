import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../EarSurgeries.module.scss";
import EarSurgeryLayout from "../EarSurgeryLayout";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import { Button } from "@ui/Button/Button";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

function Stapedectomy() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleChangeImage1 = () => {
    router.push("/EarSurgeries/Otosclerosis");
  };

  const handleChangeImage2 = () => {
    router.push("/EarSurgeries/Type01");
  };

  const handleChangeImage3 = () => {
    router.push("/EarSurgeries/Type02");
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <EarSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">Surgical removal of stapes</Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      {/* <div
                      className={styles.dotFrame}
                      style={{
                        left: "67%",
                        top: "36%",
                      }}
                    >
                      <div className={styles.dot} onClick={handleClickImage}>
                        <div className={styles.circle}></div>

                        <div className={styles.infoSurgery}>Eardrum</div>
                      </div>
                    </div> */}

                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Stapedectomy/stapes.jpg`}
                      />
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
          <Box>
            <Button className="m-1" rounded onClick={handleChangeImage1}>
              Otosclerosis
            </Button>
            <Button className="m-1" rounded onClick={handleChangeImage2}>
              Type01
            </Button>
            <Button className="m-1" rounded onClick={handleChangeImage3}>
              Type02
            </Button>
            {/* <Button type="submit" color="red" className="btn" rounded onClick={handleChangeImage1}/> */}
          </Box>
        </Box>
      </EarSurgeryLayout>
    </>
  );
}

export default Stapedectomy;
