import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
        <li>The ear is packed.</li>
        <li>The dressing on the ear is removed after 24 hours.</li>
        <li>Blood-stained drainage from the ear for a few days.</li>
        <li>Muffled hearing for a few weeks.</li>
        <li>Dizziness for about a week.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export default AfterCare;
