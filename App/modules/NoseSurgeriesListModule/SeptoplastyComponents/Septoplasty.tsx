import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../NoseSurgeries.module.scss";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import NoseSurgeryLayout from "../NoseSurgeriesLayout";
import { Button } from "@ui/Button/Button";
import AfterCare from "./AfterCare";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

function Septoplasty() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);
  const [modalShow, setModalShow] = useState(false);
  const [popupShow, setPopupShow] = useState(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleChangeImage1 = () => {
    router.push("/NoseSurgeries/SeptoplastySurgeryProdedure");
  };

  const handleChangeImage2 = () => {
    router.push("/NoseSurgeries/PostSeptoplasty");
  };

  const handlePopup = () => {
    setPopupShow(true);
    // setPopupShow(false)
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <NoseSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">
                A corrective surgical procedure to straighten the deviated nasal
                sepum (DNS)
              </Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  color: "red",
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      <div
                        className={styles.dotFrame}
                        style={{
                          left: "23.5%",
                          top: "50%",
                          // position: "relative",
                          //zIndex: draw ? 1 : 2,
                        }}
                      >
                        <div
                          className={styles.dot}
                          onClick={() => setPopupShow(!popupShow)}
                        >
                          <div className={styles.circle}></div>

                          {popupShow ? (
                            <div className={styles.infoSurgery}>
                              Nasal Septum
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Septoplasty/septoplasty.jpg`}
                      />
                      <div
                        className={`${styles.mainInfo}`}
                        style={{ left: "26%", bottom: "30px" }}
                      >
                        Normal midline nasal septum
                      </div>
                      <div
                        className={`${styles.mainInfo}`}
                        style={{ left: "75%", bottom: "30px" }}
                      >
                        The midline is off-centre
                      </div>
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
          <Box>
            <Button className="m-1" rounded onClick={handleChangeImage1}>
              Septoplasty Surgery Prodedure
            </Button>
            <Button className="m-1" rounded onClick={handleChangeImage2}>
              Post Septoplasty
            </Button>
            <Button className="m-1" rounded onClick={() => setModalShow(true)}>
              After Care
            </Button>
            <AfterCare show={modalShow} onHide={() => setModalShow(false)} />
          </Box>
        </Box>
      </NoseSurgeryLayout>
    </>
  );
}

export default Septoplasty;
