import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../NoseSurgeries.module.scss";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import NoseSurgeryLayout from "../NoseSurgeriesLayout";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

function SeptoplastySurgeryProdedure() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);
  const [popupShow, setPopupShow] = useState(false);
  const [popupShow2, setPopupShow2] = useState(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <NoseSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">Septoplasty Surgery Procedure</Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  color: "red",
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      <div
                        className={styles.dotFrame}
                        style={{
                          left: "23%",
                          top: "43.5%",
                        }}
                      >
                        <div
                          className={styles.dot}
                          onClick={() => setPopupShow(!popupShow)}
                        >
                          <div className={styles.circle}></div>
                          <div className={styles.infoSurgery}></div>
                        </div>
                      </div>
                      <div
                        className={styles.dotFrame}
                        style={{
                          left: "53%",
                          top: "42.5%",
                        }}
                      >
                        <div
                          className={styles.dot}
                          onClick={() => setPopupShow2(!popupShow2)}
                        >
                          <div className={styles.circle}></div>

                          <div className={styles.infoSurgery}></div>
                        </div>
                      </div>
                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Septoplasty/septoplasty-surgical-prodedure.jpg`}
                      />
                      {popupShow ? (
                        <div
                          className={`${styles.mainInfo}`}
                          style={{ left: "34%", bottom: "200px" }}
                        >
                          inside the nose hence no external scars.
                        </div>
                      ) : null}
                      {popupShow2 ? (
                        <div
                          className={`${styles.mainInfo}`}
                          style={{ left: "70%", bottom: "200px" }}
                        >
                          Obstruction is removed and deviation is straightened
                        </div>
                      ) : null}
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
        </Box>
      </NoseSurgeryLayout>
    </>
  );
}

export default SeptoplastySurgeryProdedure;
