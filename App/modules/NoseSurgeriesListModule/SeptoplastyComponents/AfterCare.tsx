import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
        <li>Nose may be packed.</li>
        <li>Nasal secretion/oozing for 1 or 2 days, which should be lightly sniffed into the throat and spat out.</li>
        </ul>
      </Modal.Body>
      <Modal.Header className="headerColor">
        <Modal.Title id="contained-modal-title-vcenter">
        In addition to general precautions (post-surgery)
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
        <li>Elevate head using two or three pillows.</li>
        <li>Change the nasal drip pad as often as necessary.</li>
        <li>Use ice packs on the cheeks to arrest bleeding, if necessary.</li>
        <li>Use nasal irrigations after removal of pack.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export default AfterCare;
