import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

export function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Nose may be packed.</li>
          <li>Mild pain soon after surgery.</li>
          <li>Blocked and congested nose for 2 weeks.</li>
          <li>Blood-stained discharge from the nose for 2 weeks.</li>
          <li>Improvement in symptoms in a few weeks.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export function Polyps(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Polyps</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>
            Polyp is a growth arisingfrom the mucous membrane of the nose or
            sinuses.
          </li>
        </ul>
      </Modal.Body>
      <Modal.Header className="headerColor">
        <Modal.Title id="contained-modal-title-vcenter">
          There are two type of polyps:
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Nasal</li>
          <li>Antrochoanal</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export function PolypectomyPopup(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Polypectomy
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>
            Polypectomy is done underlocal or general anaesthesia, using an
            endoscope.
          </li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}
