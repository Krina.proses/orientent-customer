import React from "react";
import NoseSurgeryLayout from "./NoseSurgeriesLayout";
import NoseSurgeriesBanner from "./NoseSurgeriesBanner";
import NoseListData from "./NoseListData";


const NoseSurgeries = () => {
  return (
    <>
      <NoseSurgeryLayout>
      {/* <Box className="row">
          <Box className="col-lg-4 col-md-6 col-sm-6 col-12"> */}
            {/* <Text>Ear Surgeries Module</Text> */}
            {/* <EarAnatomy /> */}
            <NoseSurgeriesBanner/>
            <NoseListData />
          {/* </Box> */}
          {/* <Box className="col-lg-4 col-md-6 col-sm-6 col-12">
            <Text>Ear Surgeries Module</Text>
            <EarAnatomy />
          </Box> */}
         
        {/* </Box> */}
      </NoseSurgeryLayout>
      </>
  );
};

export default NoseSurgeries;
