import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../NoseSurgeries.module.scss";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import NoseSurgeryLayout from "../NoseSurgeriesLayout";
import { Button } from "@ui/Button/Button";
import { AfterCare } from "./AfterCare";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

function FESS() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);
  const [modalShow, setModalShow] = useState(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleChangeImage1 = () => {
    router.push("/NoseSurgeries/Endoscope");
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <NoseSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">
                A minimally invasive procedure to open sinus openings and sinus
                air cells with an endoscope for the treatment of chronic
                sinusitis and nusal polyps
              </Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  color: "red",
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Endoscopy/endoscopy-operated-info.jpg`}
                      />
                      <div
                        className={`${styles.mainInfo}`}
                        style={{ left: "20%", bottom: "50px" }}
                      >
                        Non-Operated side (with sinusitis)
                      </div>
                      <div
                        className={`${styles.mainInfo}`}
                        style={{ left: "81%", bottom: "50px" }}
                      >
                        Operated Side
                      </div>
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
          <Box>
            <Button className="m-1" rounded onClick={handleChangeImage1}>
              Endoscope
            </Button>
            <Button className="m-1" rounded onClick={() => setModalShow(true)}>
              After Care
            </Button>
            <AfterCare show={modalShow} onHide={() => setModalShow(false)} />
          </Box>
        </Box>
      </NoseSurgeryLayout>
    </>
  );
}

export default FESS;
