import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

export function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>
            Nostrils are kept free of crusts by a cleaning procedure done by the
            surgeon at 2-4 days after surgery and repeated every 3-5 days for a
            few weeks.
          </li>
        </ul>
      </Modal.Body>
      <Modal.Header className="headerColor">
        <Modal.Title id="contained-modal-title-vcenter">
          In addition to general precautions :
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>
            Advised steam inhalation at least three times daily for 2 weeks.
          </li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}


export function EndoscopePopup(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Endoscope</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Endoscope is passed through the nose.</li>
          <li>sinuses are cleared and sinus opening is enlarged.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}
