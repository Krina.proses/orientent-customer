import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React from "react";
import styles from "../NoseSurgeriesListModule/NoseSurgeries.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLeaf } from "@fortawesome/free-solid-svg-icons";
import { routeBaseUrl } from "App/utils/constants";

const NoseListDataArray = [
  {
    id: 1,
    title: "Nose Anatomy",
    link: "NoseSurgeries/NoseAnatomy",
    class: "",
    svgclr: "skyblue",
    // class:'start'
    // subList: {
    //   headerTitle: "Ear Anatomy",
    //   downloadImageName: "/Ear/outer-ear-with-text.jpg",
    // },
  },
  {
    id: 2,
    title: "Septoplasty",
    link: "NoseSurgeries/Septoplasty",
    class: "",
    svgclr: "bgblue",
    // subList: {
    //   headerTitle: "Surgical repair of the eardrum or tympanic membrance(TM)",
    //   downloadImageName: "Tympanosplasty.jpg",
    // },
  },
  {
    id: 3,
    title: "FESS",
    link: "NoseSurgeries/FESS",
    class: "",
    // class: "end",
    svgclr: "bgorange",
  },
  {
    id: 4,
    title: "Polypectomy",
    link: "NoseSurgeries/Polypectomy",
    class: "",
    svgclr: "bgpink",
  },
];

function NoseListData() {
  return (
    <>
      <Box>
        <Box className={styles.bgCurve}>
        <Box className="container">
        <Box className={`${"row justify-content-center"} ${styles.leafBoxes}`}>
          {NoseListDataArray.map((item: any, index: number) => {
            return (
              <Box className="col col-2 mb-3 mt-5 justify-content-center">
                <a
                  href={`${routeBaseUrl}/${item.link}`}
                  className={`${styles.featuresltem} ${styles[item.class]}`}
                >
                  <Box className={`${styles[item.svgclr]}`} position="middle">
                    <Box>
                      <FontAwesomeIcon
                        icon={faLeaf}
                        flip="horizontal"
                        fontSize={50}
                        color="blue"
                      />
                    </Box>
                  </Box>

                  <Text className={styles.title}>{item.title}</Text>
                </a>
              </Box>
            );
          })}
        </Box>
        </Box>
        </Box>
      </Box>
    </>
  );
}

export default NoseListData;
