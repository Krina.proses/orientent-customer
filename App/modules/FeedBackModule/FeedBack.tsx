import React, { useEffect, useState } from "react";
import FeedBackLayout from "./FeedBackLayout";
import FeedBackForm from "./components/FeedBackForm";
import { useFormik } from "formik";
import * as Yup from "yup";
import { addFeedBackData } from "App/api/api";
import { toastAlert } from "App/shared/components/Layout";
import { handleServerError } from "App/utils/helpers";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import styles from './FeedBack.module.scss'
function FeedBack() {
  const [submitAttempt, setSubmitAttempt] = useState<boolean>(false);

  useEffect(() => {}, []);

  const initialValuesForFeedBack: any = {
    doctor_name: "",
    mobile_no: "",
    email: "",
    review: "",
    doctor_id: "",
  };

  const courseRegistrationValidationSchema = Yup.object().shape({
    doctor_name: Yup.string().required("Full Name is required"),
    mobile_no: Yup.number()
      .min(10)
      .required("Mobile Number is required")
      .typeError("Mobile must be a number"),
    email: Yup.string()
      .required("Email is required")
      .email("Please Enter Valid Email")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Please Enter Valid Email"
      ),
    review: Yup.string().required("Review is required"),
  });

  const { values, handleChange, handleSubmit, errors } = useFormik({
    initialValues: { ...initialValuesForFeedBack },
    validationSchema: courseRegistrationValidationSchema,
    enableReinitialize: true,
    onSubmit: (values: any, { resetForm }) => {
      handleFormSubmmit(values, resetForm);
    },
  });

  async function handleFormSubmmit(values: any, resetForm: any) {
    try {

      let user: any = localStorage.getItem("user_data");
      user = JSON.parse(user)
      
      
      let formData = {
        ...values,
        doctor_id: user.doctor_id
      };
      // return
      const feedBackData: any = await addFeedBackData(formData);
      console.log(feedBackData,"feedBackData");
      
      resetForm();
      toastAlert("success", feedBackData.data.msg);

    } catch (error) {
      console.log(error,"errorrrrrrr");
      
      handleServerError(error);
    }
  }

  return (
    <FeedBackLayout>
      <Box className="text-center">
        <Text size="h2">Feedback Form</Text>
      </Box>
      <Box className={`${styles.featuresltem}`}>
      <FeedBackForm
        istrationForm
        handleChange={handleChange}
        values={values}
        setSubmitAttempt={setSubmitAttempt}
        errors={errors}
        submitAttempt={submitAttempt}
      />
      <Button color="green"
      className={`${styles.bdrRadius} btn-sm me-3`}
            type="submit"
            onClick={(e: any) => {
              handleSubmit(e);
              setSubmitAttempt(true);
            }}
          >
            Submit
          </Button>
      {/* <Box className="container">
        <Box className="d-flex justify-content-end">
          
        </Box>
      </Box> */}
      </Box>
    </FeedBackLayout>
  );
}

export default FeedBack;
