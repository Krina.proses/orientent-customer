import React from "react";
import Box from "@ui/Box/Box";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import Textarea from "@ui/Textarea";
import { StyledCard } from "@ui/Card/Card.styles";
import styles from "../FeedBack.module.scss";
import { Button } from "@ui/Button/Button";
import { faLeaf, faBarsStaggered } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAndroid } from "@fortawesome/free-brands-svg-icons";
function FeedBackForm({ values, handleChange, submitAttempt, errors }: any) {
  return (
    <>
      {/* <Box className={styles.featuresltem}> */}

      <Box className="col">
        <Input
          label="Full Name"
          required
          name="doctor_name"
          value={values?.doctor_name}
          onChange={handleChange}
          error={submitAttempt && errors.doctor_name}
        />
      </Box>

      <Box className="col">
        <Input
          label="Mobile"
          required
          name="mobile_no"
          value={values?.mobile_no}
          onChange={handleChange}
          error={submitAttempt && errors.mobile_no}
        />
      </Box>

      <Box className="col">
        <Input
          label="Email"
          required
          name="email"
          value={values?.email}
          onChange={handleChange}
          error={submitAttempt && errors.email}
        />
      </Box>

      <Box className="col">
        <Textarea
          label="Review"
          required
          name="review"
          value={values?.review}
          onChange={handleChange}
          error={submitAttempt && errors.review}
        ></Textarea>
      </Box>
     
      {/* </Box> */}
    </>
  );
}

export default FeedBackForm;
