import Box from '@ui/Box/Box';
import Layout from 'App/shared/components/Layout';
import { useRouter } from 'next/router';

const FeedBackLayout = ({children}:{children: React.ReactNode }) => {
  const router = useRouter();
  const pathname = router.pathname;
  return (
    <>
    <Layout>
      <Box className="container">
      <Box className="row row-cols-1 justify-content-center">
        <Box className="col col-4">
      {children}
      </Box>
      </Box>
      </Box>
    </Layout>
    </>
  );
}



export default FeedBackLayout;