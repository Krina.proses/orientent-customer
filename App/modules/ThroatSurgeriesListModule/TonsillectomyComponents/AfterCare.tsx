import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

export function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Pain, on and off, for about a week before disappearing completely</li>
          <li>Bleeding may occur in some patients during the first week but usually stops on its own, rarely requiring medical attention</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}