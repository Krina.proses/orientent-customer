import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React, { useRef, useState } from "react";
import styles from "../ThroatSurgeries.module.scss";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import CanvasDraw from "react-canvas-draw";
import Pen from "App/orienticons/Pen";
import Cross from "App/orienticons/Cross";
import Undo from "App/orienticons/Undo";
import { useRouter } from "next/router";
import { Button } from "@ui/Button/Button";
import ThroatSurgeryLayout from "../ThroatSurgeriesLayout";
import { publicPathName } from "App/utils/constants";
import BackArrow from "App/orienticons/BackArrow";

function Phonosurgery() {
  const router = useRouter();

  const firstCanvas = useRef<any>(null);

  const [draw, setDraw] = useState<boolean>(false);

  const clear = () => {
    firstCanvas.current.clear();
    setDraw(false);
  };
  const undo = () => {
    firstCanvas.current.undo();
  };

  const handleChangeDraw = () => {
    setDraw(true);
  };

  const handleChangeImage1 = () => {
    router.push("/ThroatSurgeries/Thyroplasty");
  };

  const handleChangeImage2 = () => {
    router.push("/ThroatSurgeries/InjectionLaryngoplasty");
  };

  const handleChangeImage3 = () => {
    router.push("/ThroatSurgeries/Phonomicrosurgery");
  };

  const handleBackHistory = () => {
    router.back();
  };

  return (
    <>
      <ThroatSurgeryLayout>
        <Box className="container">
          <Box className="row text-center align-items-center">
            <Box className="col text-md-start">
              <BackArrow
                onClick={handleBackHistory}
                className={styles.button}
              />
            </Box>
            <Box className="col col-9">
              <Text className="topHeading">
                A group of surgical procedures designed to alter, improve or
                maintain the voice
              </Text>
            </Box>
            <Box className="col text-md-end">
              {draw ? (
                <>
                  <Undo onClick={undo} className={styles.button} />
                  <Cross onClick={clear} className={styles.button} />
                </>
              ) : (
                <Pen onClick={handleChangeDraw} className={styles.button} />
              )}
            </Box>
          </Box>
          <Box>
            <Box className={styles.surgery}>
              <CanvasDraw
                ref={firstCanvas}
                brushRadius={2}
                brushColor="#FF0000"
                canvasHeight={650}
                lazyRadius={0}
                className={styles.canvasSize}
                disabled={draw ? false : true}
                gridColor="transparent"
                style={{
                  borderRadius: 1,
                  color: "red",
                  backgroundColor: "transparent",
                  position: "absolute",
                  zIndex: draw ? 2 : 1,
                }}
              />
              <div
                style={{
                  position: "relative",
                  zIndex: draw ? 1 : 2,
                }}
              >
                <TransformWrapper initialScale={1}>
                  <TransformComponent>
                    <div className={styles.imageFrame}>
                      <img
                        className="img-fluid"
                        width="100%"
                        src={`${publicPathName}/Phonosurgery/normal-vocal-cords.jpg`}
                      />
                    </div>
                  </TransformComponent>
                </TransformWrapper>
              </div>
            </Box>
          </Box>
          <Box>
            <Button className="m-1" rounded onClick={handleChangeImage1}>
              Thyroplasty
            </Button>
            <Button className="m-1" rounded onClick={handleChangeImage2}>
              Injection Laryngoplasty
            </Button>
            <Button className="m-1" rounded onClick={handleChangeImage3}>
              Phonomicrosurgery
            </Button>
          </Box>
        </Box>
      </ThroatSurgeryLayout>
    </>
  );
}

export default Phonosurgery;
