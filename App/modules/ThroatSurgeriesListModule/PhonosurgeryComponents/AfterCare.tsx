import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

export function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header  closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Precautions after Laryngeal Surgery :
        </Modal.Title>
      </Modal.Header>
      <Modal.Header className="headerColor">
        <Modal.Title id="contained-modal-title-vcenter">DOs</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <ul>
          <li>Voice rest for 48 hours.</li>
          <li>Use voice only when necessary for 1 week.</li>
          <li>Eat slowly to avoid coughing or choking.</li>
          <li>Take the prescribed medications.</li>
          <li>Keep follow-up appointments.</li>
        </ul>
      </Modal.Body>
      <Modal.Header className="headerColor">
        <Modal.Title id="contained-modal-title-vcenter">DON'Ts</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <ul>
          <li>Cough for a few days.</li>
          <li>Strain or lift any heavy weight for a few weeks.</li>
          <li>Smoke or drink alcohol.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export function InjectionLaryngoplastyAfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Injection Laryngoplasty
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <ul>
          <li>
            Injection Laryngoplasty is a technique of delivering a substance
            into the vocal cord.
          </li>
          <li>
            The injection may be given using an endoscope or through the skin on
            neck.
          </li>
          <li>
            It is indicated in vocal cord paralysis (fat, collagen, teflon,
            gelform, calcium hydroxylapatite, etc.,injected) and laryngeal
            stenosis (steroid injected).
          </li>
          <li>Done under general or local anaesthesia.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}

export function PhonomicrosurgeryAfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          After-Care and Points to Note
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <ul>
          <li>Dressing around the neck for 24 hours.</li>
          <li>A drain from the wound for 1-2 days.</li>
          <li>
            Pain in the throat or mouth, which is relieved by medications.
          </li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}
