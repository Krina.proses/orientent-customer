import React from "react";
import ThroatSurgeryLayout from "./ThroatSurgeriesLayout";
import ThroatSurgeriesBanner from "./ThroatSurgeriesBanner";
import ThroatListData from "./ThroatListData";

const NoseSurgeries = () => {
  return (
    <>
      <ThroatSurgeryLayout>
        <ThroatSurgeriesBanner />
        <ThroatListData />
      </ThroatSurgeryLayout>
    </>
  );
};

export default NoseSurgeries;
