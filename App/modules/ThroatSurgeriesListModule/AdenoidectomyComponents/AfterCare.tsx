import React, { useRef } from "react";
import Modal from "react-bootstrap/Modal";

export function AfterCare(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="headerColor" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
        Points to Note
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Pain and discomfort in the throat for 1 or 2 days.</li>
          <li>Congestion in the nose, which resolves on its own in a few days or weeks.</li>
          <li>Voice changes, which normalises in 2-4 weeks.</li>
        </ul>
      </Modal.Body>
    </Modal>
  );
}