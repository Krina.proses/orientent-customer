import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import styles from "./Resources.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderClosed } from "@fortawesome/free-solid-svg-icons";
import { getResources } from "App/api/api";
import { handleServerError } from "App/utils/helpers";

// const ResourcesArray = [
//   {
//     id: 1,
//     title: "Uncommon Cold",
//     link: "/",
//     class: "",
//     svgclr: "bgorange",
//     // class:'start'
//     // subList: {
//     //   headerTitle: "Ear Anatomy",
//     //   downloadImageName: "/Ear/outer-ear-with-text.jpg",
//     // },
//   },
//   {
//     id: 2,
//     title: "Nasal Surgery",
//     link: "/",
//     class: "",
//     svgclr: "bgpink",
//     // subList: {
//     //   headerTitle: "Surgical repair of the eardrum or tympanic membrance(TM)",
//     //   downloadImageName: "Tympanosplasty.jpg",
//     // },
//   },
//   {
//     id: 3,
//     title: "Ear Surgery",
//     link: "/",
//     class: "",
//     // class: "end",
//     svgclr: "bgblue",
//   },
//   {
//     id: 4,
//     title: "Throat Surgery",
//     link: "/",
//     class: "",
//     svgclr: "skyblue",
//   },
// ];

function ResourcesData() {

  const [resourcesList, setResourcesList] = useState<any>([]);


  
  useEffect(() => {
    getAllResources();
  }, []);

  const getAllResources = async () => {
    try {

      let PDF = await getResources();
      
      setResourcesList(PDF.data.data);
    } catch (error) {
      handleServerError(error);
    }
  };

  return (
    <Box>
      <Box className={styles.bgCurve}>
      <Box className="container">
        <Box className={`${"row justify-content-center"} ${styles.leafBoxes}`}>
          {resourcesList.map((item: any, index: number) => {
            return (
              <Box className="col col-2 mb-3 mt-5 justify-content-center">
                <a
                  href={`https://orientcounsellingtool.cipla.com:9050/resources/${item.name}`}
                  className={`${styles.featuresltem} ${styles[item.class]}`}
                  target="_blank"
                >
                  <Box className={`${styles[item.svgclr]}`} position="middle">
                    <Box>
                      <FontAwesomeIcon
                        icon={faFolderClosed}
                        flip="horizontal"
                        fontSize={50}
                        color="blue"
                      />
                      {/* <FontAwesomeIcon icon={faFolderClosed} /> */}
                    </Box>
                  </Box>

                  <Text className={styles.title}>{item.title}</Text>
                </a>
              </Box>
            );
          })}
        </Box>
      </Box>
      </Box>
    </Box>
  );
}

export default ResourcesData;
