import React from "react";
import ResourcesLayout from "./ResourcesLayout";
import ResourcesData from "./ResourcesData";
import ResourcesBanner from "./ResourcesBanner";

const Resources = () => {
  return (
    <ResourcesLayout>
      <ResourcesBanner />
      <ResourcesData />
    </ResourcesLayout>
  );
};

export default Resources;
