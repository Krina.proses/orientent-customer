
import Box from '@ui/Box/Box'
import React from 'react'
import styles from "./Resources.module.scss";
import Text from '@ui/Text/Text';

const ResourcesBanner = () => {
  return (
   <>
 
   <Box>
   <Box className={`innerbanner ${styles.innerbanner}`}>
   <Text className={styles.bannerTitle}>Resources</Text>
   {/* <img
            className="img-fluid"
            width="100%"
            src="/ear.jpg"
            
          /> */}
   </Box>
   </Box>
  
   </>
  )
}

export default ResourcesBanner

