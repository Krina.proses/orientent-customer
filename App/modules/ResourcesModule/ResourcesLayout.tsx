
import React from 'react'
import { useRouter } from "next/router";
import Box from '@ui/Box/Box';
import Layout from 'App/shared/components/Layout';
import Header from 'App/shared/components/Header/Header';
const ResourcesLayout = ({children}:{children: React.ReactNode }) => {
    const router = useRouter();
    const pathname = router.pathname;
  return (
    <>
     <Layout>
     <Box>
        {/* <Box className="row"> */}
          {/* <Box className="col"> */}
            {children}
          {/* </Box> */}
        {/* </Box> */}
      </Box>
     </Layout>

    </>
  )
}

export default ResourcesLayout