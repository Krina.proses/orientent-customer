import Layout from "App/shared/components/Layout";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import style from "./Home.module.scss";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import ImageSection from "./components/ImageSection";

function Home() {
  const router = useRouter();


  return (

    

    <Layout>
    
      <Box className="row">
        <Box className="col">
          <ImageSection/>
        </Box>
      </Box>
    </Layout>
  );
}

export default Home;
