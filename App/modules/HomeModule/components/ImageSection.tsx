import Box from "@ui/Box/Box";
import React, { useState } from "react";
import styles from "../Home.module.scss";
import { publicPathName, routeBaseUrl } from "App/utils/constants";

const ImageSection = () => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [previousMouseX, setPreviousMouseX] = useState<number | null>(null);
  const [isMouseDown, setIsMouseDown] = useState<boolean>(false);

  const images: string[] = [
    "/humanHead/Head-01.jpg",
    "/humanHead/Head-02.jpg",
    "/humanHead/Head-03.jpg",
    "/humanHead/Head-04.jpg",
    "/humanHead/Head-05.jpg",
    "/humanHead/Head-06.jpg",
    "/humanHead/Head-07.jpg",
    "/humanHead/Head-08.jpg",
    "/humanHead/Head-09.jpg",
    "/humanHead/Head-10.jpg",
    "/humanHead/Head-11.jpg",
    "/humanHead/Head-12.jpg",
    "/humanHead/Head-13.jpg",
    "/humanHead/Head-14.jpg",
    "/humanHead/Head-15.jpg",
    "/humanHead/Head-16.jpg",
    // Add your image paths here...
  ];

  const earArr: string[] = [
    "18%",
    "78%",
    "75%",
    "57%",
    "50%",
    "42%",
    "28%",
    "19%",
    "13%",
    "74%",
    "64%",
    "53%",
    "46%",
    "31%",
    "21%",
    "21%",
    "21%",
    // Add your coordinates here...
  ];

  const earArrReverse = [
    "18%",
    "18%",
    "78%",
    "75%",
    "57%",
    "50%",
    "42%",
    "28%",
    "19%",
    "13%",
    "74%",
    "64%",
    "53%",
    "46%",
    "31%",
    "21%",
    "21%",
    "21%",
  ];

  const noseArr = [
    "46%",
    "28%",
    "13%",
    "5%",
    "5%",
    "10%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "83%",
    "87%",
    "87%",
    "79%",
    "65%",
  ];

  const noseReverseArr = [
    "46%",
    "46%",
    "28%",
    "13%",
    "5%",
    "5%",
    "10%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "83%",
    "87%",
    "87%",
    "79%",
    "65%",
  ];

  const throatArr = [
    "49%",
    "46%",
    "44%",
    "34%",
    "34%",
    "34%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "56%",
    "56%",
    "55%",
    "55%",
    "54%",
  ];

  const throatArrReverse = [
    "49%",
    "49%",
    "46%",
    "44%",
    "34%",
    "34%",
    "34%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "-1000%",
    "56%",
    "56%",
    "55%",
    "55%",
    "54%",
  ];

  // Define other coordinate arrays...

  const handleMouseDown = () => {
    setIsMouseDown(true);
  };

  const handleMouseUp = () => {
    setIsMouseDown(false);
  };

  const handleMouseMove = (event: React.MouseEvent) => {
    event.preventDefault();

    if (isMouseDown) {
      const currentMouseX = event.clientX;

      if (previousMouseX !== null) {
        // if (currentMouseX > previousMouseX) {
        //   // (prevIndex - 1) % images.length || 16
        //   setCurrentIndex((prevIndex) => (prevIndex - 1) % images.length || 16);
        // } else if (currentMouseX < previousMouseX) {
        //   setCurrentIndex((prevIndex) => (prevIndex + 1) % images.length);
        // }

        if (currentMouseX > previousMouseX) {
          // setCurrentIndex((prevIndex) => {
          //   console.log(prevIndex, "prevIndex"); // Log prevIndex
          //   return (prevIndex - 1 + images.length) % images.length;
          // });

          setCurrentIndex((prevIndex) => (prevIndex + 1) % images.length);

          // setCurrentIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);
        } else if (currentMouseX < previousMouseX) {
          // setCurrentIndex((prevIndex) => {
          //   console.log(prevIndex, "prevIndex"); // Log prevIndex
          //   return (prevIndex + 1) % images.length;
          // });
          // setCurrentIndex((prevIndex) => (prevIndex + 1) % images.length);
          setCurrentIndex(
            (prevIndex) => (prevIndex - 1 + images.length) % images.length
          );
        }
      }

      setPreviousMouseX(currentMouseX);
    }
  };

  return (
    <Box
      className={`${styles.imagecontainer}`}
      style={{ position: "relative" }}
    >
      {/* <div> */}
      {/* <div id="ear" className={styles.dotcircle} style={{top: "44%", left: earArr[currentIndex]}}>
                <div className={styles.dotinner}><div className={styles.dotring}></div></div>
                <div className={styles.dotboxup}>
                    Ear Anatomy & Surgeries
                </div>
            </div>
            <div id="nose" className={styles.dotcircle}>
                <div className={styles.dotinner}><div className={styles.dotring}></div></div>
                <div className={styles.dotboxdown}>
                    Nose Anatomy & Surgeries
                </div>
            </div>
            <div id="throat" className={styles.dotcircle}>
                <div className={styles.dotinner}><div className={styles.dotring}></div></div>
                <div className={styles.dotboxdown}>
                    Throat Anatomy & Surgeries
                </div>
            </div>
          <img
            id="image"
            src={images[currentIndex]}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
            onMouseMove={handleMouseMove}
            alt="Image"
          /> */}
      {/* <div
            id="ear"
            style={{ top: earArr[currentIndex], left: earArr[currentIndex], right: earArr[currentIndex], bottom: earArr[currentIndex] }}
          >
          </div> */}
      {/* <div
            id="ear"
            style={{ top: earArrReverse[currentIndex], left: earArrReverse[currentIndex], right: earArrReverse[currentIndex], bottom: earArrReverse[currentIndex] }}
          >
          </div> */}
      {/* <div
            id="nose"
            style={{ top: noseArr[currentIndex], left: noseArr[currentIndex], right: noseArr[currentIndex], bottom: noseArr[currentIndex] }}
          >
          </div> */}
      {/* <div
            id="nose"
            style={{ top: noseReverseArr[currentIndex], left: noseReverseArr[currentIndex], right: noseReverseArr[currentIndex], bottom: noseReverseArr[currentIndex] }}
          >
          </div> */}
      {/* <div
            id="throat"
            style={{ top: throatArr[currentIndex], left: throatArr[currentIndex], right: throatArr[currentIndex], bottom: throatArr[currentIndex] }}
          >
          </div> */}
      {/* <div
            id="throat"
            style={{ top: throatArrReverse[currentIndex], left: throatArrReverse[currentIndex], right: throatArrReverse[currentIndex], bottom: throatArrReverse[currentIndex] }}
          >
          </div> */}
      {/* </div> */}
          <div>
      <img className="img-fluid" src={`${publicPathName}/humanHead/Head-01.jpg`} alt="sculp" />
      <a href={`${routeBaseUrl}/EarSurgeries`}>
        <div className={styles.dotcircle} style={{ top: "45%", left: "19%" }}>
          <div className={styles.dotinner}>
            <div className={styles.dotring}></div>
          </div>
         
              <div className={styles.dotbox}>Ear Anatomy & Surgeries</div>
        </div>
        </a>
        <a href={`${routeBaseUrl}/NoseSurgeries`}>
        <div className={styles.dotcircle} style={{ top: "47.5%", left: "47.5%" }}>
          <div className={styles.dotinner}>
            <div className={styles.dotring}></div>
          </div>
              <div className={styles.dotbox}>Nose Anatomy & Surgeries</div>
        </div>
        </a>
        <a href={`${routeBaseUrl}/ThroatSurgeries`}>
        <div className={styles.dotcircle} style={{ top: "72.5%", left: "49%" }}>
          <div className={styles.dotinner}>
            <div className={styles.dotring}></div>
          </div>
          <div className={styles.dotbox}>Throat Anatomy & Surgeries</div>
        </div>
      </a>
      </div>
    </Box>
  );
};

export default ImageSection;
