import Box from "@ui/Box/Box";
import React, { useContext, useEffect, useState } from "react";
import LoginForm from "./components/LoginForm";
import style from "./Login.module.scss";
import Card from "@ui/Card/Card";
import { handleServerError } from "App/utils/helpers";
import { LoginData } from "App/api/api";
import Text from "@ui/Text/Text";
import CryptoJS from "crypto-js";
import { CRYPTOJS_CODE, LS_KEYS, TOKEN_PREFIX, publicPathName } from "App/utils/constants";
import { useRouter } from "next/router";
import { toastAlert } from "App/shared/components/Layout";
import AppLink from "./AppLink";
import Router from "next/router";
import AuthContext from "App/contexts/auth/auth.context";

function Login() {
  const router = useRouter();

  const { authDispatch }: any = useContext(AuthContext);

  const [isMobile, setIsMobile] = useState<boolean>(false);
  
  const [otp, setOtp] = useState<any>(null);
  const [loading, setLoading] = useState(false);


  // useEffect(() => {
  //   const userAgent = window.navigator.userAgent;
  //   console.log(userAgent, "userAgentuserAgentuserAgentuserAgent");
    
  //   if (userAgent.includes('Mobile')) {
  //     // It's likely a mobile device
  //   } else {
  //     // It's likely a desktop web browser
  //   }
  //   // getMenuList();
  // }, []);


  useEffect(() => {
    const checkDeviceType = () => {
      if (window.innerWidth <= 1024) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    };

    // Initial check when the component mounts
    checkDeviceType();

    // Listen for window resize events
    window.addEventListener('resize', checkDeviceType);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('resize', checkDeviceType);
    };
  }, []);


  const handleLoginDetailsData = async () => {
    setLoading(true);
    try {
      if (!otp) {
        setLoading(false);
        return toastAlert("error", "Please Enter 6-Digit Code");
      }

      let ciphertext = CryptoJS.AES.encrypt(`${otp}`, CRYPTOJS_CODE).toString();

      // let deviceid = "ab123";
      // let deviceId = CryptoJS.AES.encrypt(
      //   `${deviceid}`,
      //   CRYPTOJS_CODE
      // ).toString();

      let formValues = {
        six_digit_code: ciphertext,
        // device_id: deviceId,
      };

      let result: any = await LoginData(formValues);


      // if (result.data.msg == "Code is used already") {
      //   setLoading(false);
      //   return toastAlert("error", result.data.msg);
      // }
      if (result.data.msg == "Your code has not been generated") {
        setLoading(false);
        return toastAlert("error", result.data.msg);
      }

      localStorage.setItem(TOKEN_PREFIX, result.data.data.token);
      localStorage.setItem(
        LS_KEYS.userData,
        JSON.stringify(result.data.data.data)
      )

      authDispatch({ type: "SIGN_IN" });
      router.replace("/Home");
      toastAlert("success", result.data.msg);
      setLoading(false);
      // router.push("/Home");
    } catch (error) {
      setLoading(false);
      handleServerError(error);
    }
  };

  return (
    <>
      <Box className="container">
      <div>
      {isMobile ? (
        <AppLink />
      ) : (
        // <p>This website is open on a laptop or desktop.</p>
        <Card shadow>
        <Box align="center" className={style.logincenter}>
          <Box className="text-center">
            <img width={266} height={79} src={`${publicPathName}/orntlogo.png`} alt="logo" />
          </Box>
          <Box className="mt-5">
            <Text size="h2">Enter Your 6-Digit Code</Text>
          </Box>
          <LoginForm
            handleLoginDetailsData={handleLoginDetailsData}
            setOtp={setOtp}
            otp={otp}
            loading={loading}
          />
        </Box>
      </Card>
      )}
    </div>
       
      </Box>
    </>
  );
}

export default Login;
