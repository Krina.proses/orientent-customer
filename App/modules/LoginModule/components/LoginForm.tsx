import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import OTPInput from "react-otp-input";
import styles from '../Login.module.scss'
function LoginForm({ setOtp, otp, handleLoginDetailsData, loading }: any) {
  return (
    <>
      <div className="container">
        <Box className="row row-cols-1">
          <Box className="col">
            <Box className="d-flex justify-content-center">
              <OTPInput
                value={otp}
                onChange={(e) => {
                  setOtp(e);
                }}
                numInputs={6}
                inputType={"number"}
                renderSeparator={<span>&nbsp;&nbsp;</span>}
                renderInput={(props) => <input {...props} className="w-30" />}
              />
            </Box>
          </Box>

          <Box className="col text-center">
            {loading ? (
              <Box className="loading" style={{ position: "relative", transform: "scale(0.4)" }}></Box>
            ) : (
              <Button color="green"
      className={`${styles.bdrRadius} btn-sm me-3`}
              auto
              // className="radiusBtn"
              type="submit"
              onClick={handleLoginDetailsData}
            >
              {" "}
              Submit{" "}
            </Button>
            )}
           
          </Box>
        </Box>
      </div>
    </>
  );
}

export default LoginForm;
