import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import React, { useEffect, useState } from "react";
import styles from "./Login.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAndroid, faApple } from "@fortawesome/free-brands-svg-icons";
function AppLink() {
  return (
    <>
      <div className="d-flex align-items-center justify-content-center vh-100 text-center p-0">
        <div>
          <Box>
            <Button
              type="button"
              className={`${styles.bdrRadius} mb-2`}
            >
              <Box flex className={`${styles.btnLabeled}`}>
                {" "}
                <FontAwesomeIcon
                  fontSize={20}
                  icon={faAndroid}
                  color="white"
                />
              </Box>
              Android
            </Button>
          </Box>
          <Box>
          <a
          href="https://apps.apple.com/app/orient-ent-counselling-app/id1574473891"
          target="_blank"
        >
            <Button
              type="button"
              className={`${styles.bdrRadius}`}
              // className="btn d-flex flex-row btn-labeled btn-success"
            >
              <Box className={`${styles.btnLabeled}`}>
                {" "}
                <FontAwesomeIcon
                  fontSize={20}
                  icon={faApple}
                  color="white"
                  // className={styles.greenicon}
                />
              </Box>
              IOS
            </Button>
            </a>
          </Box>
          {/* <Box className="text-center">
      
        <a href="" target="_blank">
          <Text>
            <strong>For Android</strong>
          </Text>
        </a>
       
        <a
          href="https://apps.apple.com/app/orient-ent-counselling-app/id1574473891"
          target="_blank"
        >
          <Text>
            <strong>For IOS</strong>
          </Text>
        </a>
      </Box> */}
        </div>
      </div>
    </>
  );
}

export default AppLink;
