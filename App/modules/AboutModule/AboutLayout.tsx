import Layout from 'App/shared/components/Layout';
import { useRouter } from 'next/router';

const AboutLayout = ({children}:{children: React.ReactNode }) => {
  const router = useRouter();
  const pathname = router.pathname;
  return (
    <>
    <Layout>
      {children}
    </Layout>
    </>
  );
}



export default AboutLayout;