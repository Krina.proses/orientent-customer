import React from "react";
import Text from "@ui/Text/Text";
import Box from "@ui/Box/Box";
import styles from "./About.module.scss";
const AboutContent = () => {
  return (
    <>
      <Box className={styles.aboutContent}>
        <Text>
          ENT ("Ear, Nose, Throat") surgery is a medical specialty practiced by
          ENT surgeons. 'Otolaryngologists', as they are referred to in the
          medical community, can be a bit of a tongue twister for the lay
          person, which is why they are better known as ENT specialists. The
          term is also self-explanatory, as it dervies from their area of
          expertise - the ear, nose and throat. This includes related structures
          like the sinuses and larynx. While general physicians may be able to
          treat problems that affect these areas, they can only offer medical
          treatment. ENT specialists are physicians trained in both the medical
          and surgical management of such health conditions, which is why they
          are also referred to as Head and Neck Surgeons.
        </Text>
      </Box>
    </>
  );
};

export default AboutContent;
