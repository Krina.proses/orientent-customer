import React from "react";
import AboutLayout from "./AboutLayout";

import AboutContent from "./AboutContent";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
const About = () => {
  return (
    <>
      <AboutLayout>
        <Box className="container">
          <Box>
            <Text size="h2"><strong>About :</strong></Text>
          </Box>
          <Box className="col">
            <AboutContent />
          </Box>
        </Box>
      </AboutLayout>
    </>
  );
};

export default About;
