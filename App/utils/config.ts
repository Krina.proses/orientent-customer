//@ts-nocheck
let configData = {
  development: {
    // ApiUrl: "https://orientcounsellingtool.cipla.com:9050",
    ApiUrl: "http://localhost:9005",
    adminURL: "http://localhost:4200",
    baseUrl: "",
    publicBasePath: ""
  },
  // staging: {
  //   ApiUrl: "https://orientcounsellingtool.cipla.com:9050",
  //   adminURL: "https://isccm.org/admin-control-dev",
  //   baseUrl: "",
  //   publicBasePath: ""
  // },
  production: {
    // customerURL: "http://prosesindia.in/orientent-customer",
    ApiUrl: "http://prosesindia.in:9005",
    adminURL: "http://prosesindia.in/orient-surgery/#/auth",
    baseUrl: "/orientent-customer",
    publicBasePath: "/orientent-customer"
  },
};

export default function (env: string): {
  ApiUrl: string;
  adminURL: string;
  baseUrl: string;
  publicBasePath: string;
} {
  return configData[env];
}
