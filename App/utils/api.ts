import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from "axios";
import CryptoJS from "crypto-js";
import { CRYPTOJS_CODE, NODE_API_URL, TOKEN_PREFIX } from "./constants";
import { formatError } from "./helpers";
import { getRefreshToken } from "App/api/api";

const fetchClient = () => {
  const defaultOptions = {
    baseURL: NODE_API_URL,
    headers: {
      "Content-Type": "application/json",
    },
  };

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(async (config: any) => {
    try {
      let token = null;
      if (typeof window !== undefined) {
        token = localStorage.getItem(TOKEN_PREFIX);
      }
      config.headers.Authorization = token;
    } catch (error) {
      console.log(error);
    }
    return config;
  });

  instance.interceptors.response.use(
    (response: AxiosResponse) => response,
    async (err: any) => {
      console.log(err.response, "err.response");

      let token = null;
      token = localStorage.getItem(TOKEN_PREFIX);

      if (err?.response?.status === 401 && token != null) {
        const newAccessToken = await refreshAccessToken();

        if (newAccessToken) {
          // Store the new access token
          localStorage.setItem(TOKEN_PREFIX, newAccessToken);

          // Retry the original request with the new token
          return instance(err.config);
        } else {
          window.location.href = "Login";
          localStorage.removeItem(TOKEN_PREFIX);
          localStorage.removeItem("user_data");
        }
      }
      return Promise.reject(err.response);
    }
  );

  return instance;
};

const refreshAccessToken = async () => {
  try {
    let user: any = localStorage.getItem("user_data");
    user = JSON.parse(user);

    let ciphertext = CryptoJS.AES.encrypt(
      `${user.six_digit_code}`,
      CRYPTOJS_CODE
    ).toString();

    // let deviceId = CryptoJS.AES.encrypt(
    //   `${user.device_id}`,
    //   CRYPTOJS_CODE
    // ).toString();

    let body: any = {
      six_digit_code: ciphertext,
      // device_id: deviceId,
    };

    const response = await getRefreshToken(body);

    const newAccessToken = response.data.data.token;

    // Store the new access token
    localStorage.setItem(TOKEN_PREFIX, newAccessToken);

    return newAccessToken;
  } catch (error) {
    console.log(error, "errorrrrrrrrrrr");

    // Handle refresh token failure
    return null;
  }
};

export default fetchClient();
