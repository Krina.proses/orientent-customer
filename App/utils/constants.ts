// import environment from "App/environment";
import environment from "../environment";
import configFunc from "./config";
const config = configFunc(environment);

export const NODE_API_URL = config.ApiUrl;

export const TOKEN_PREFIX = `${NODE_API_URL}ORIENT__`;

export const TOKEN_S = "@_ISCCM_JWT@@$$$$";

export const LS_KEYS = {
  userData: 'user_data',
}

export const CRYPTOJS_CODE = "sada092121[*]#piup"

export const publicPathName = `${config.publicBasePath}`

export const routeBaseUrl = `${config.baseUrl}`



