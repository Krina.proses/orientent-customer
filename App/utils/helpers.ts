
//@ts-nocheck
// import { getLS } from "@ui/DataGrid/utils";
import environment from "App/environment";
import { toastAlert } from "App/shared/components/Layout";
import config from "./config";
import { MESSAGES, MENU_PREFIX } from "./constants";
import HttpStatusCode from "./httpStatus";

//validate fields if empty or not
export const validFields = (fields: any) => {
  let i = 0;
  let arr = Object.values(fields);
  let len = arr.length;
  let result = true;
  while (i < len) {
    if (!arr[i]) {
      result = false;
      break;
    }
    i++;
  }

  return result;
};

//validate email
export const validateEmail = (email: string) => {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
};

export const getMessageFromStatus = (
  status: HttpStatusCode,
  msg: string | null
) => {
  switch (status) {
    case 401:
      return MESSAGES.INVALID_LOGIN;
    case 409:
      return msg ? msg : MESSAGES.ALREADY_EXIST;
    default:
      return MESSAGES.SERVER_ERROR;
  }
};

//format err
export const formatError = (err: any) => {
  let val: any = { msg: "", status: null, field: null };
  if (typeof err?.data == "string") {
    val.msg = getMessageFromStatus(err?.status, err?.data);
    val.status = err.status;
  } else if (err?.data?.err?.errors) {
    //format sequelize error
    let e = err?.data?.err?.errors;

    val = [];
    for (let iterator of e) {
      val.push({
        msg: iterator.message,
        field: iterator.path,
        status: err.status,
      });
    }
  } else if (err?.data?.required) {
    //format express validator error
    let e = err?.data?.required.errors;

    val = [];
    for (let iterator of e) {
      val.push({
        msg: iterator.msg,
        field: iterator.param,
        status: err.status,
      });
    }
  } else {
    val.msg = getMessageFromStatus(err?.status, null)
      ? getMessageFromStatus(err?.status, null)
      : err.data.msg;
    val.status = err?.status;
  }
  return val;
};

export const cond = (arr: any[]) => {
  if (!arr.length) {
    return;
  }
  if (arr[0]) {
    return arr[1];
  } else {
    return arr[2];
  }
};

// for formate date
export const dateFormater = (dateValue: Date) => {
  return new Date(dateValue).toLocaleDateString('en-GB');
};

// for yes no date
export const yesnoFormater = (yesNoValue: 0 | 1) => {
  return yesNoValue == 0 ? "No" : "Yes";
};

//format active or not
// export const activeFormatter = (value: 0 | 1) => {
//   return value ? "Active" : "Inactive";
// };
// 

export const formatDate = (date: any, format: string = "") => {
  var d = new Date(date),
  month = "" + (d.getMonth() + 1),
  day = "" + d.getDate(),
  year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  if (format == "m-d-y") {
    return [month, year, day].join("-");
  } else if (format == "d-m-y") {
    return [day, month, year].join("-");
  } else if (format == "d-m") {
    return [day, month].join("-");
  } else if (format == "d/m/y") {
    return [day, month, year].join("/");
  } else {
    return [year, month, day].join("-");
  }
};
export const formatDateWIthMonthName = (date: any, format: string = "") => {
  var d = new Date(date),
  month = d.toLocaleString('default', { month: 'long' }),
  day = d.getDate(),
  year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  const nth = function (day) {
    if (day > 3 && day < 21) return day = day + 'th';
    switch (d % 10) {
      case 1: return day = day + "st";
      case 2: return day = day + "nd";
      case 3: return day = day + "rd";
      default: return day = day + "th";
    }
  }

  day = nth(day);

  if (format == "m-d-y") {
    return [month, year, day].join("-");
  } else if (format == "d-m-y") {
    return [day, month, year].join("-");
  } else if (format == "d-m") {
    return [day, month].join("-");
  } else if (format == "d/m/y") {
    return [day, month, year].join("/");
  } else if (format == "y/m/d") {
    return [year, month, day].join("-");
  } else {
    return [day, month, year].join(" ");
  }
};
export const formatDateWIthMonthNameShort = (date: any, format: string = "") => {
  var d = new Date(date),
    month = d.toLocaleString('default', { month: 'short' }),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  if (format == "m-d-y") {
    return [month, year, day].join("-");
  } else if (format == "d-m-y") {
    return [day, month, year].join("-");
  } else if (format == "d-m") {
    return [day, month].join("-");
  } else if (format == "d/m/y") {
    return [day, month, year].join("/");
  } else {
    return [year, month, day].join("-");
  }
};

export const monthFromDate = (date) => {
  var d = new Date(date);
  let month = d.toLocaleString('default', { month: 'short' });
  return month
}

// //get action permission using path
// export const getActionPermission = (path: string): any => {
//   let perm = null;
//   if (!path) {
//     return perm;
//   }

//   const MENU = getLS(MENU_PREFIX);

//   if (!MENU && !MENU?.length) {
//     return perm;
//   }

//   MENU.map((parent: any) => {
//     if (parent?.children?.length) {
//       parent.children.map((child: any) => {
//         if (child.link == path) {
//           perm = child?.permission || null;
//           return;
//         }
//       });
//     } else if (parent.link == path) {
//       perm = parent?.permission || null;
//       return;
//     }
//   });
//   return perm;
// };



// generate unique number
export const getUniqueNumber = (prefix: string | null) => {
  var date = new Date();
  var components = [
    prefix,
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
    date.getMilliseconds(),
  ];

  return components.join("");
};

// checking object is empty or not
export const isEmptyObj = (obj: any): boolean => {
  return Object.keys(obj).length === 0;
};

export const getAdminLink = (url: string) => {
  let c = config(environment);
  return `${c.adminURL}/#${url}`
}

export const handleServerError = (error: any) => {
  if (!error || error == "undefined" || error == undefined) {
    toastAlert("error", "Somthing went wrong!");
  }

  if (error?.status === 409 || error?.status === 500) {
    toastAlert("error", error?.data?.msg ? error?.data?.msg : error?.data);
  }

  if (error?.status === 404 || error?.status === 401 || error?.status === 400) {
    console.log(error, "error")
    toastAlert("error", error?.data);
  }

  if (error?.status === 422) {
    toastAlert("error", error?.data?.required?.errors[0]?.msg);
  }
}

export const dateFormateValue = (date: any) => {  

  const inputDate = formatDate(date, "d-m-y");
  const dateParts = inputDate.split("-");

  const dateObj = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
  const options = { day: "numeric", month: "long", year: "numeric" };

  const formattedDate = dateObj.toLocaleDateString("en-GB", options);

  return formattedDate
}
