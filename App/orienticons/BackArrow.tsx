import React from "react";
// import WithIcons from "../icons/withProps";
import WithIcons from "./withProps";

function BackArrow(props: any) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-arrow-left ${props.className}`}
      viewBox="0 0 16 16"
    >
      <path
        fill-rule="evenodd"
        d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
      />
    </svg>
  );
}

export default WithIcons(BackArrow);
