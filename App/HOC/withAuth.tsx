import React, { useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import AuthContext from "App/contexts/auth/auth.context";
// import { getActionPermission } from "App/utils/helpers";

function WithAuth(Comp: any) {
  return function WrappedWithToast(props: any) {
    const { authState }: any = useContext(AuthContext);
    const router = useRouter();
    const [permission, setpermission] = useState({});
    useEffect(() => {
      if (!authState?.authenticated) {
        router.push("/Login");
      }

      // const permOBJ = getActionPermission(router.pathname);
      // if (!permOBJ && !permOBJ?.length) {
      //   // router.push("/Login");
      // }

      // setpermission(permOBJ);
    }, [authState]);

    return <Comp {...{ ...props, permission: permission }} />;
  };
}

export default WithAuth;
