import { getSession } from "next-auth/react";

export async function getLoginSession(context: any) {
  const session = await getSession(context);
  if (!session) {
    return {
      redirect: {
        destination: "/Login",
        permanent: false,
      },
    };
  }
  return {
    props: {
      session,
    },
  };
}
