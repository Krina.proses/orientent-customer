import api from "App/utils/api"




export const LoginData = (body: any) => {
    return api.post("doctor/WebDoctorLogin", body)
}

export const LogoutData = (body: any) => {
    return api.post("doctor/webLogout", body)
}


/////   feedback api
export const addFeedBackData = (body: any) => {
    return api.post("feedback/add ", body)
}

//// reresources

export const getResources = () => {
    return api.get("resources/all")
}

export const getRefreshToken = (body: any) => {    
    return api.post("doctor/webAuth", body)
}

