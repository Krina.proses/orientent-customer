import Box from '@ui/Box/Box';
import Link from 'next/link';
import Image from "next/image";
import React from 'react';
import styles from "./Header.module.scss";
import { publicPathName } from 'App/utils/constants';

function Logo() {
    return (

        <Box className={`${styles.logoSection}`}>
            <Link href={`/Home`}>
                <a>
                    <img width={135} height={50} src={`${publicPathName}/orntlogo.png`} alt="logo" />
                </a>
            </Link>
        </Box>

    )
}

export default React.memo(Logo)
