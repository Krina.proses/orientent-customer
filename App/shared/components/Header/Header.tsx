import Box from "@ui/Box/Box";
import clsx from "clsx";
import { NextRouter, useRouter } from "next/router";
import { memo, useEffect, useState } from "react";
import OrientMenu from "../Menu/OrientMenu";
import styles from "./Header.module.scss";
import Logo from "./Logo";



function Header({}: any) {
  // const { uiState, uiDispatch } = useContext(UiContext);
  const router: NextRouter = useRouter();
  const [title, setTitle] = useState<string>("Admin");
  const [activeLink, setActiveLink] = useState<string>("Home");

  const [customerInfo, setCustomerInfo] = useState<any>({});
  const [screenWidth, setScreenWidth] = useState<number>(0);

  useEffect(() => {
    if (localStorage.getItem("user_data")) {
      //@ts-ignore
      setCustomerInfo(JSON.parse(localStorage.getItem("user_data")));
      console.log(router.pathname);
      setActiveLink(router.pathname);
    }

    const hasWindow = typeof window !== "undefined";
    const width = hasWindow ? window.innerWidth : 0;
    setScreenWidth(width);
  }, []);

  return (
    <header
      className={clsx(styles.header, {
        // [styles.headerClose]: !uiState.sidebarOpen,
      })}
    >
      <Box className={`container ${styles.headerContainer}`}>
        <Logo />
        <Box className={`${styles.headerContainerCenter}`}>
          <OrientMenu />
        </Box>
      </Box>
    </header>
  );
}

export default memo(Header);
