import UiContext from "App/contexts/ui/ui.context";
import { useRouter, NextRouter } from "next/router";
import clsx from "clsx";
import styles from "./Footer.module.scss";
import Image from "next/image";
import Text from "App/ui/Text/Text";
import {
  DropdownGroup,
  MainMenu,
  TriggerButton,
  TriggerMenu,
} from "./Footer.styles";
import { useContext, useState } from "react";
import Link from "next/link";
import Input from "@ui/Input/Input";
import { Button } from "@ui/Button/Button";


//action type for dropdown

function Footer(props: any) {
  // const { uiState, uiDispatch } = useContext(UiContext);
  const router: NextRouter = useRouter();
  const [title, setTitle] = useState<string>("Admin");

  const handleChange = (id: string | number) => {
    switch (id) {
      case 1:
        adminProfile();
        break;
      case 3:
        logout();
        break;
      case 2:
        ChangePassword();
        break;
      default:
        break;
    }
  };

  const logout = () => {
    localStorage.clear();
    router.push("/Login");
  };

  const adminProfile = () => {
    router.push("/Profile");
  };

  const ChangePassword = () => {
    router.push("/ChangePass");
  };

  return (
    <footer className={`${styles.footer}`}>
      {/* <div className={`${styles.footerContainer}`}> */}
      {/* <div className="container-fluid p-0"> */}
        {/* <div className={`row ${styles.footerContainer}`}> */}
          {/* <img className={styles.footerLogo} width={91} height={50} src={`/isccm/logo.png`} alt="logo" /> */}
          {/* <div className={`col text-left col-12 col-md-6 ${styles.footerContainerCenter}`}>
            <div className={`my-2 ${styles.logoSection}`}>
              <Link href={`/`}>
                <a>
                  <img width={221} height={27} src={`/isccm/logo.svg`} alt="logo" />
                </a>
              </Link>
            </div>
          </div> */}
          {/* <div className="col text-center pe-3">
            <div className={`my-2 ${styles.footerMenu}`}>
              <Link href={`/`}>Home</Link> |{" "}
             
              <Link href="/TermsConditions"><a target="_blank">Terms & Conditions</a></Link> |{" "}
              <Link href="/PrivacyPolicy"><a target="_blank">Privacy Policy</a></Link> |{" "}
              <Link href="/ContactUs"><a target="_blank">Contact us</a></Link>
            </div>
          </div> */}
        {/* </div> */}
      
      {/* </div> */}
      {/* </div> */}
      <div className={`${styles.footerCopyright}`}>
        <div className="container">
          © 2023-24 Orient | Fushing ideas with experience
        </div>
      </div>
      {/* </div> */}
      {/* </div> */}
    </footer>
  );
}

export default Footer;
