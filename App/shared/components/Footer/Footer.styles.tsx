import Box from "App/ui/Box/Box";
import { styled } from "App/theme/stitches.config";


export const FooterContainer = styled('footer', {
  boxShadow: "none",
  // display: "flex",
  // flexDirection: "column",
  width: "100%",
  boxSizing: "border-box",
  flexShrink: 0,
  // position: "fixed",
  // zIndex: 1100,
  // top: "$0",
  // left: "auto",
  // right:"$0",
  color: "$whiteA",
  
  // left: auto,
  // right: 0px,
  // color: rgb(255, 255, 255),
  // transition: all 0.3s ease-in-out,
})

export const MainMenu = styled(Box, {
  display: 'flex',
  alignItems: "center"
})

export const DropdownGroup = styled(Box, {
  display: "flex",
  gap: "$5"
})

export  const TriggerMenu = styled('a', {

})
export  const TriggerButton = styled('button', {

    all: "unset",
    fontFamily: "inherit",
    // borderRadius: "100%",
    display: "inline-block",
    alignItems: "center",
    justifyContent: "center",
    color: "$pText1",
    $$bs: "$colors$indigo5",
    "&:hover": { 
      // backgroundColor: "$bgHover",
      opacity: 0.9 
    },
    "&:focus": { 
      // backgroundColor: "$bgActive" 
    },
    variants: {
      size: {
        user: {
          width: 24
        },
      },
      color: { 
        default: {
          backgroundColor: "$indigo9",
          color: "$whiteA12"
        },
        gradient: {
          backgroundImage: "$gradient",
          color: "$whiteA12"
        },
        user: {
          backgroundColor: "#ff0000",
          color: "$whiteA12"
        },
        clear: {
          backgroundColor: "transparent",
          color: "$whiteA12"
        },
      },
      rounded: {
        true: {
          br: '$sm'
        }
      },
    }
  })
  