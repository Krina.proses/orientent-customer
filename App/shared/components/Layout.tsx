import Box from "@ui/Box/Box";
// import { getCustomerMenu } from "App/api/customerMenu";
import Header from "App/shared/components/Header/Header";
import { handleServerError } from "App/utils/helpers";
import { useRouter } from "next/router";
import React, { FC, useEffect, useState } from "react";
import { toast } from "react-toastify";
import Footer from "./Footer/Footer";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false;
// method for alert();
export const toastAlert = (type: string, msg: string) => {
  if (type == "success") return toast.success(msg);
  else if (type == "warn") return toast.warn(msg);
  else if (type == "error") return toast.error(msg);
  else return toast.info("An Alert Problem");
};

const Layout: FC = ({ children }) => {
  const router = useRouter();
  const pathname = router.pathname;
  // console.log(pathname, "pathname")

  const [customerMenu, setcustomerMenu] = useState<any>([]);
  const [pageName, setPageName] = useState<any>();
  const [mainMenuID, setMainMenuID] = useState<number | null>(null);
  const [selectedMainMenuID, setSelectedMainMenuID] = useState<number | null>(
    null
  );
  const [menuIndex, setMenuIndex] = useState<number>(0);
  const [selectedMenuIndex, setSelectedMenuIndex] = useState<number>(0);

  useEffect(() => {
    // getMenuList();
  }, []);

  // const getMenuList = async () => {
  //   try {
  //     let menu = await getCustomerMenu();
  //     // console.log(menu.data.data);
      
  //     setcustomerMenu(menu.data.data);

  //     // console.log(mainMenuID)
  //     // console.log(pathname)
  //     // console.log(menu.data.data);      

  //     let tempUrlArray = pathname.split("/");

  //     let page = getPageName(tempUrlArray[1]);
  //     if(!page) {
  //     page = getPageName(tempUrlArray[2]);
  //     }
  //     setPageName(page);

  //     if (tempUrlArray[1] === "ContactUs") {
  //       setPageName("Contact Us");

  //     } else {
  //       let mainMenu = menu.data.data.find(
  //         (mainMenu: any) => mainMenu.link == tempUrlArray[1]
  //       );
  //       //      console.log(mainMenu, "mainMenu")
  //       setSelectedMainMenuID(mainMenu.id);
  //       let menuIndex = mainMenu.children.findIndex((menu: any) =>
  //         menu.children.find((subMenu: any) => subMenu.link == pathname)
  //       );

  //       let page = mainMenu.children[menuIndex].children?.find(
  //         (item: any) => item.link == pathname
  //       );
  //       // console.log(page.title, "page.title")
  //       setPageName(page.title);
  //     }
      

  //     //    console.log(menuIndex ,"menuIndex")
  //     if (menuIndex > 0) {
  //       setMenuIndex(menuIndex);
  //       setSelectedMenuIndex(menuIndex);
  //     } else {
  //       setMenuIndex(0);
  //       setSelectedMenuIndex(0);
  //     }
  //   } catch (error) {
  //     handleServerError(error);
  //   }
  // };

  return (
    <>
    <div className="mainPage">
      <Header />
      {/* {console.log(pageName, "pageName")} */}
      {/* {pathname != "/" ? (
        <Box className="pageTitleBg">
          <Box className="container">
            <PageHeader title={pageName} />
          </Box>
        </Box>
      ) : null} */}

      <div className="wrapper">
        {/* <Sidebar /> */}
        <div id="content">{children}</div>
        {/* use for alert */}
      
      </div>
      <Footer />
      </div>
    </>
  );
};

export default Layout;
