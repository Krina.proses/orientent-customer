import React, { useState } from "react";
import DashboardIcon from "App/orienticons/DashboardIcon";
import NoseIcon from "App/orienticons/NoseIcon";
import EarIcon from "App/orienticons/EarIcon";
import FolderIcon from "App/orienticons/FolderIcon";
import ChatIcon from "App/orienticons/ChatIcon";
import LogoutIcon from "App/orienticons/LogoutIcon";
import InfoIcon from "App/orienticons/InfoIcon";
import { faBarsStaggered } from "@fortawesome/free-solid-svg-icons";
import { faFacebookF } from "@fortawesome/free-brands-svg-icons";
import { LogoutData } from "App/api/api";
import { CRYPTOJS_CODE, TOKEN_PREFIX, publicPathName, routeBaseUrl } from "App/utils/constants";
import { useRouter } from "next/router";
import { handleServerError } from "App/utils/helpers";
import CryptoJS from "crypto-js";
import style from "./Orientmenu.module.scss";
// import component 👇
import Drawer from "react-modern-drawer";

//import styles 👇
import "react-modern-drawer/dist/index.css";
import { Button } from "@ui/Button/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Text from "@ui/Text/Text";

const OrientMenu = () => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState);
  };
  const [isSubMenuOpen, setIsSubMenuOpen] = useState(false);

  const toggleSubMenu = () => {
    setIsSubMenuOpen(!isSubMenuOpen);
  };
  const logoutData = async () => {
    try {
      let userData: any = localStorage.getItem("user_data");
      userData = JSON.parse(userData);

      let ciphertext = CryptoJS.AES.encrypt(
        `${userData.six_digit_code}`,
        CRYPTOJS_CODE
      ).toString();

      let formValues = {
        six_digit_code: ciphertext,
      };

      let logout = await LogoutData(formValues);
      console.log(logout, "logout");

      localStorage.removeItem(TOKEN_PREFIX);
      localStorage.removeItem("user_data");
      router.push(`/Login`);
    } catch (error) {
      console.log(error, "errorrrrrrrrr");

      handleServerError(error);
    }
  };
  return (
    <>
      {/* <button onClick={toggleDrawer}>Show</button> */}
      <a href="https://www.facebook.com/groups/orient2017/" target="_blank">
        <Button className="btn btn-sm btn-secondary">
          <FontAwesomeIcon
            fontSize={20}
            icon={faFacebookF}
            className={style.facebookicon}
          />
          {/* <Box> */}
          <Text className={style.orientFB} color="white">
            Orient Facebook Page{" "}
          </Text>
          {/* </Box> */}
        </Button>
      </a>
      <Button
        auto
        className={style.svgclr}
        // className={'${"navbar-toggler-icon btn-light"} ${styles.bg-black}'}
        type="button"
        onClick={toggleDrawer}
        color="clear"
      >
        <FontAwesomeIcon
          fontSize={20}
          icon={faBarsStaggered}
          className={style.greenicon}
        />

        {/* <FontAwesomeIcon
                        icon={faLeaf}
                        flip="horizontal"
                        fontSize={50}
                        color="black"
                      /> */}
      </Button>
      <Drawer
        open={isOpen}
        onClose={toggleDrawer}
        direction="right"
        // className='bla bla bla'
        // className={style.}
      >
        <div className={style.sidebar}>
          <ul>
            <li>
              <a href={`${routeBaseUrl}/Home`}>
                <DashboardIcon /> Home
              </a>
            </li>
            <li>
              <a href={`${routeBaseUrl}/EarSurgeries`}>
                <EarIcon /> Ear Surgeries List
              </a>
            </li>
            {/* <li>
            <a href="/EarSurgeries/EarAnatomy">
                <NoseIcon /> Nose Sergeries List
              </a>
            </li> */}
            <li>
              <a href={`${routeBaseUrl}/NoseSurgeries`}>
                <NoseIcon /> Nose Surgeries List
              </a>
            </li>
            <li>
              <a href={`${routeBaseUrl}/ThroatSurgeries`}>
                <img
                  className={style.imgicon}
                  height={18}
                  width={18}
                  src={`${publicPathName}/mouth.png`}
                  alt="logo"
                />
                Throat Surgeries List
              </a>
            </li>
            <li>
              {" "}
              <a href={`${routeBaseUrl}/Resources`}>
                <FolderIcon /> Resources
              </a>
            </li>
            <li>
              <a href={`${routeBaseUrl}/FeedBack`}>
                <ChatIcon /> Feedback
              </a>
            </li>
            <li>
              <a href={`${routeBaseUrl}/About`}>
                <InfoIcon /> About
              </a>
            </li>
            <li>
              <a onClick={logoutData} className={style.logout}>
                <LogoutIcon /> Logout
              </a>
            </li>
          </ul>
        </div>
      </Drawer>
      {/* <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <button
            type="button"
            className="navbar-toggler"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <div className="navbar-nav">
              <a href="/Home" className="nav-item nav-link active">
                <DashboardIcon /> Home
              </a>
              <a href="/EarSurgeries" className="nav-item nav-link">
                <EarIcon /> Ear Surgeries List
              </a>
              <a href="/NoseSurgeries" className="nav-item nav-link">
                <NoseIcon /> Nose Sergeries List
              </a>
              <a href="#" className="nav-item nav-link">
                <img width={25} height={15} src={`/mouth.png`} alt="logo" />{" "}
                Throat Sergeries List
              </a>
              <a href="#" className="nav-item nav-link">
                <FolderIcon /> Resources
              </a>
              <a href="#" className="nav-item nav-link">
                <ChatIcon /> Feedback
              </a>
              <a href="#" className="nav-item nav-link">
                <InfoIcon /> About
              </a>
            </div>
            <div className="navbar-nav ms-auto">
              <Box onClick={logoutData} className={style.logout}>
                <LogoutIcon /> Logout
              </Box>
            </div>
          </div>
        </div>
      </nav> */}
    </>
  );
};

export default OrientMenu;
