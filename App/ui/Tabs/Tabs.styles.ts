//@ts-nocheck
import { violet, mauve, blackA, green } from '@radix-ui/colors';
import * as TabsPrimitive from '@radix-ui/react-tabs';
import { colors } from 'react-select/dist/declarations/src/theme';
import { styled } from '../../theme/stitches.config';

const Tabs = styled(TabsPrimitive.Root, {
  display: 'flex',
  // flexDirection: 'column',
  // width: 300,
  // boxShadow: `0 2px 10px ${blackA.blackA4}`,
});

const StyledList = styled(TabsPrimitive.List, {
  flexShrink: 0,
  // display: 'flex',
  // borderBottom: `1px solid ${mauve.mauve6}`,
});

const StyledTrigger = styled(TabsPrimitive.Trigger, {
  all: 'unset',
  fontFamily: 'inherit',
  backgroundColor: 'white',
  padding: '0 20px',
  margin: '0',
  width: '100%',
  height: 45,
  // flex: 1,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: 15,
  lineHeight: 1,
  color: mauve.mauve11,
  userSelect: 'none',
  // '&:first-child': { borderTopLeftRadius: 6 },
  // '&:last-child': { borderTopRightRadius: 6 },
  '&:hover': { color: '$prime' },
  '&[data-state="active"]': {
    color: '$prime',
    boxShadow: 'inset 0 -1px 0 0 currentColor, 0 1px 0 0 currentColor',
  },
  // '&:focus': { position: 'relative', boxShadow: `0 0 0 2px black` },
});

const StyledContent = styled(TabsPrimitive.Content, {
  flexGrow: 1,
  padding: 20,
  backgroundColor: 'white',
  // borderBottomLeftRadius: 6,
  // borderBottomRightRadius: 6,
  outline: 'none',
  // '&:focus': { boxShadow: `0 0 0 2px black` },
});

// Exports
export const StyledTabs = Tabs;
export const TabsList = StyledList;
export const TabsTrigger = StyledTrigger;
export const TabsContent = StyledContent;