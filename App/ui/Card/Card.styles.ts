import { styled } from "../../theme/stitches.config";


export const StyledCard = styled('div', {
  margin: "$4",
  padding: "$4",
  borderRadius: "$lg",
  position: 'relative',
  // display: 'inline',
  // overflow: 'hidden',
  // height: 'auto',
  height: '100%',
  boxSizing: 'border-box',
  '&.variant':{
    marginBottom: '25px',
    cursor: 'pointer',
    '&.selected':{
      backgroundColor: "#f7f7f7",
      boxShadow: "0px 3px 6px rgba(0,0,0,0.16), 0px 3px 6px rgba(0,0,0,0.16) inset"
    },
  },
  variants: {
    color: {
      default: {
        backgroundColor: "$whiteA12"
      },
      clear: {
        backgroundColor: "transparent"
      },
      gray: {
        backgroundColor: "#f7f7f7"
      },

      },
      //bordered
    bordered: {
        true: {
          borderStyle: 'solid',
          borderColor: '$gray6'
        },
        false: {
          borderWidth: 0
        }
      },
      //clickable
      clickable: {
        true: {
          cursor: 'pointer',
          us: 'none',
          WebkitTapHighlightColor: 'transparent',
          '&:focus:not(&:focus-visible)': {
            boxShadow: 'none'
          },
          '&:focus': {
            outline: 'none',
          },
          '@safari': {
            WebkitTapHighlightColor: 'transparent',
            outline: 'none'
          }
        }
      },
      //shadow
      shadow: {
        true: {
          boxShadow: "$sm"
        }
      },
      //hoverable
      hoverable: {
        true: {
          '&:hover': {
            transform: 'translateY(-2px)',
            boxShadow: '$md'
          }
        }
      },
      animated: {
        true: {
          transition: '$default'
        },
        false: {
          transition: 'none'
        }
      },
  },
  compoundVariants: [
    // clickable && animated
    {
      clickable: true,
      animated: true,
      css: {
        '&:active': {
          scale: 0.97
        }
      }
    }
  ],
  defaultVariants: {
    color: 'default',
    animated: true,
    bordered: false,
    shadow: false,
    clickable: false
  }
})