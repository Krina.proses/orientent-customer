import React from 'react'
import { StyledText, TextVariantsProps } from './Text.styles'
import type * as Stitches from '@stitches/react';


type textProps = {children :React.ReactNode} & TextVariantsProps & {className?: string|null, css?: Stitches.CSS, onClick?: any, size?: string, color?: string ,font?: string, onMouseEnter?: any, weight?: string}

function Text({children,className, ...rest}: textProps) {
  return (
    <StyledText {...rest} className={className ? className : ''} >
      {children}
    </StyledText>
  )
}

export default Text