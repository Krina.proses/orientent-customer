//@ts-nocheck
import { cssFocusVisible, styled, VariantProps } from "../../theme/stitches.config";

export const StyledText = styled(
    'p', {
      // my: 0,
      variants: {
        fontName: {
          PoppinsRegular: {
            fontFamily: '$PoppinsRegular',
          },
          PoppinsNormal: {
            fontFamily: '$PoppinsNormal',
          },
          LoraRegular: {
            fontFamily: '$LoraRegular',
          }
        },
        font: {
          normal: {
            // fontFamily: '$Nunito'
            fontWeight: "$normal"
          },
          extraLight: {
            fontWeight: "$hairline"

          },
          semiBold: {
            fontWeight:"$semibold"
          },
          bold: {
            fontWeight: "$bold"

          }
        },
        size: {
          h1: {
            fontSize: '$xl',
            lineHeight: '$xl',
          },
          h2: {
            fontSize: '$lg',
            lineHeight: '$lg',
          },
          h3: {
            fontSize: '$md',
            lineHeight: '$md',
          },
          h4: {
            fontSize: '$sm',
            lineHeight: '$sm',
          },
          h5: {
            fontSize: '$base',
            lineHeight: '$md',
          },
          h6: {
            fontSize: '$xs',
            lineHeight: '$md',
          },
          h7:{
            fontSize:'$tiny',
            lineHeight: '$md',
          }
        },
        color: {
          default: {
            color: "$textColor"
          },
          primary: {
            color: "$pText1"
          },
          secondary: {
            color: "$secondary"
          },
          dark: {
            color: "$dark"
          },
          white: {
            color: "$whiteColor"
          },
          error: {
            color: "red"
          },
        },
        weight: {
          hairline: {
            fontWeight: '$hairline'
          },
          thin: {
            fontWeight: '$thin'
          },
          light: {
            fontWeight: '$light'
          },
          normal: {
            fontWeight: '$normal'
          },
          medium: {
            fontWeight: '$medium'
          },
          semibold: {
            fontWeight: '$semibold'
          },
          bold: {
            fontWeight: '$bold'
          },
          extrabold: {
            fontWeight: '$extrabold'
          },
        },
        style: {
          normal: {
            fontStyle: 'normal'
          },
          italic: {
            fontStyle: 'italic'
          },
        },
        transform: {
          uppercase: {
            textTransform: 'uppercase'
          },
          lowercase: {
            textTransform: 'lowercase'
          },
          capitalize: {
            textTransform: 'capitalize'
          },
        },
        decoration: {
          overline: {
            textDecoration: 'overline'
          },
          linethrough: {
            textDecoration: 'line-through'
          },
          underline: {
            textDecoration: 'underline'
          },
        },
        shadow: {
          true: {
            ts: '$sm'
          }
        },
        link: {
          true: {
            cursor: 'pointer',
            color: 'red',
            "&:hover": {
              color: 'orange'
            }
          }
        },
      },
      defaultVariants: {
        color: 'default',
        weight: 'normal',
        font: 'normal',
        style: 'normal',
        // size: 'h5',
      }
    },
    cssFocusVisible
  );

  export type TextVariantsProps = VariantProps<typeof StyledText>;
