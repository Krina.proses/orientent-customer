import * as LabelPrimitive from "@radix-ui/react-label";
import { styled, VariantProps } from "../../theme/stitches.config";

export const StyledLabel = styled(LabelPrimitive.Root, {
  userSelect: "none",
  lineHeight: 1.5,
  fontSize: '$sm',
  // paddingLeft: 15,
  // marginBottom: 5,
  display: "inline-block",
  variants: {
    showCursor: {
      true: {
        cursor: "pointer",
      },
    },
  },
});

export const RequiredSpan = styled("span", {
  color: "red",
 
});

export type LabelVariantProps = VariantProps<typeof StyledLabel>;
