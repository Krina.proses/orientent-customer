
import WithAuth from 'App/HOC/withAuth'
import EarSurgeriesModule from 'App/modules/EarSurgeriesListModule/EarSurgeries'
import React from 'react'

const EarSurgeries = () => {
  return (
   <>
   <EarSurgeriesModule/>
   </>
  )
}

export default WithAuth(EarSurgeries)

