

import type { NextPage } from "next";
import AboutModule from 'App/modules/AboutModule/About'
import WithAuth from "App/HOC/withAuth";
const About: NextPage = () => {
  return (
   <>
   <AboutModule/>
   </>
  )
}

export default WithAuth(About)

