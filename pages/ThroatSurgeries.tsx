import WithAuth from 'App/HOC/withAuth'
import ThroatSurgeriesModule from 'App/modules/ThroatSurgeriesListModule/ThroatSurgeries'
import React from 'react'

const ThroatSurgeries = () => {
  return (
   <ThroatSurgeriesModule/>
  )
}

export default WithAuth(ThroatSurgeries)