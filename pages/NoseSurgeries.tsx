import WithAuth from 'App/HOC/withAuth'
import NoseSurgeriesModule from 'App/modules/NoseSurgeriesListModule/NoseSurgeries'
import React from 'react'

const NoseSurgeries = () => {
  return (
   <NoseSurgeriesModule/>
  )
}

export default WithAuth(NoseSurgeries)