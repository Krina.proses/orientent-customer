
import WithAuth from 'App/HOC/withAuth';
import OtosclerosisModule from 'App/modules/EarSurgeriesListModule/StapedectomyComponents/Otosclerosis'
import React from 'react'

function Otosclerosis() {
  return (
   <>
   <OtosclerosisModule />
   </>
  )
}

export default WithAuth(Otosclerosis);

