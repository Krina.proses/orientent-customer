
import WithAuth from 'App/HOC/withAuth';
import ZMeatplastyModule from 'App/modules/EarSurgeriesListModule/MastoidectomyComponents/ZMeatplasty'
import React from 'react'

function ZMeatplasty() {
  return (
   <>
   <ZMeatplastyModule />
   </>
  )
}

export default WithAuth(ZMeatplasty);

