
import WithAuth from 'App/HOC/withAuth';
import Type02Module from 'App/modules/EarSurgeriesListModule/StapedectomyComponents/Type02'
import React from 'react'

function Type02() {
  return (
   <>
   <Type02Module />
   </>
  )
}

export default WithAuth(Type02);

