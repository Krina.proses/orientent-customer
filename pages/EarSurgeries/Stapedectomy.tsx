
import WithAuth from 'App/HOC/withAuth';
import StapedectomyModule from '../../App/modules/EarSurgeriesListModule/StapedectomyComponents/Stapedectomy'
import React from 'react'

function Stapedectomy() {
  return (
   <>
   <StapedectomyModule />
   </>
  )
}

export default WithAuth(Stapedectomy);

