
import WithAuth from 'App/HOC/withAuth';
import EardrumViewrModule from 'App/modules/EarSurgeriesListModule/TympanoplastyComponents/EardrumView'
import React from 'react'

function EardrumView() {
  return (
   <>
   <EardrumViewrModule />
   </>
  )
}

export default WithAuth(EardrumView);

