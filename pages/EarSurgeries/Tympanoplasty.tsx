
import WithAuth from 'App/HOC/withAuth';
import TympanoplastyModule from '../../App/modules/EarSurgeriesListModule/TympanoplastyComponents/Tympanoplasty'
import React from 'react'

function Tympanoplasty() {
  return (
   <>
   <TympanoplastyModule />
   </>
  )
}

export default WithAuth(Tympanoplasty);

