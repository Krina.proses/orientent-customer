
import WithAuth from 'App/HOC/withAuth';
import MastoidectomyModule from 'App/modules/EarSurgeriesListModule/MastoidectomyComponents/Mastoidectomy'
import React from 'react'

function Mastoidectomy() {
  return (
   <>
   <MastoidectomyModule />
   </>
  )
}

export default WithAuth(Mastoidectomy);

