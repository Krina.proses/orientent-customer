import WithAuth from 'App/HOC/withAuth';
import PorpModule from 'App/modules/EarSurgeriesListModule/OssiculoplastyComponents/Porp'
import React from 'react'

function Porp() {
  return (
   <>
   <PorpModule />
   </>
  )
}

export default WithAuth(Porp);

