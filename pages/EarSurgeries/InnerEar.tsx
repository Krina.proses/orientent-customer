
import WithAuth from 'App/HOC/withAuth';
import InnerEarModule from 'App/modules/EarSurgeriesListModule/EarAnatomyComponents/InnerEar'
import React from 'react'

function InnerEar() {
  return (
   <>
   <InnerEarModule />
   </>
  )
}

export default WithAuth(InnerEar);

