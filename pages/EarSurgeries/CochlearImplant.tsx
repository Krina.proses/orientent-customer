
import WithAuth from 'App/HOC/withAuth';
import CochlearImplantModule from 'App/modules/EarSurgeriesListModule/CochlearImplantComponents/CochlearImplant'
import React from 'react'

function CochlearImplant() {
  return (
   <>
   <CochlearImplantModule />
   </>
  )
}

export default WithAuth(CochlearImplant);

