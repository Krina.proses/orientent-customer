
import WithAuth from 'App/HOC/withAuth';
import SimpleModule from 'App/modules/EarSurgeriesListModule/MastoidectomyComponents/Simple'
import React from 'react'

function Simple() {
  return (
   <>
   <SimpleModule />
   </>
  )
}

export default WithAuth(Simple);

