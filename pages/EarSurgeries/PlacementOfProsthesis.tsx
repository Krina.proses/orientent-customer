
import WithAuth from 'App/HOC/withAuth';
import PlacementOfProsthesisModule from 'App/modules/EarSurgeriesListModule/CochlearImplantComponents/PlacementOfProsthesis'
import React from 'react'

function PlacementOfProsthesis() {
  return (
   <>
   <PlacementOfProsthesisModule />
   </>
  )
}

export default WithAuth(PlacementOfProsthesis);

