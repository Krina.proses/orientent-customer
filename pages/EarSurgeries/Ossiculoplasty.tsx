import WithAuth from 'App/HOC/withAuth';
import OssiculoplastyModule from 'App/modules/EarSurgeriesListModule/OssiculoplastyComponents/Ossiculoplasty'
import React from 'react'

function Ossiculoplasty() {
  return (
   <>
   <OssiculoplastyModule />
   </>
  )
}

export default WithAuth(Ossiculoplasty);

