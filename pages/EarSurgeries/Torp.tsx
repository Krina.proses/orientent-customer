import WithAuth from 'App/HOC/withAuth';
import TorpModule from 'App/modules/EarSurgeriesListModule/OssiculoplastyComponents/Torp'
import React from 'react'

function Torp() {
  return (
   <>
   <TorpModule />
   </>
  )
}

export default WithAuth(Torp);

