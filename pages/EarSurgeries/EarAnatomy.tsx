
import WithAuth from 'App/HOC/withAuth';
import EarAnatomyModule from '../../App/modules/EarSurgeriesListModule/EarAnatomyComponents/EarAnatomy'
import React from 'react'

function EarAnatomy() {
  return (
   <>
   <EarAnatomyModule />
   </>
  )
}

export default WithAuth(EarAnatomy);

