
import WithAuth from 'App/HOC/withAuth';
import RadicalModule from 'App/modules/EarSurgeriesListModule/MastoidectomyComponents/Radical'
import React from 'react'

function Radical() {
  return (
   <>
   <RadicalModule />
   </>
  )
}

export default WithAuth(Radical);

