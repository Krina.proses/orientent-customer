
import WithAuth from 'App/HOC/withAuth';
import Type01Module from 'App/modules/EarSurgeriesListModule/StapedectomyComponents/Type01'
import React from 'react'

function Type01() {
  return (
   <>
   <Type01Module />
   </>
  )
}

export default WithAuth(Type01);

