import WithAuth from 'App/HOC/withAuth';
import TypesOfProsthesesModule from 'App/modules/EarSurgeriesListModule/OssiculoplastyComponents/TypesOfProstheses'
import React from 'react'

function TypesOfProstheses() {
  return (
   <>
   <TypesOfProsthesesModule />
   </>
  )
}

export default WithAuth(TypesOfProstheses);

