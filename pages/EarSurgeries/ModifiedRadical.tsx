
import WithAuth from 'App/HOC/withAuth';
import ModifiedRadicalModule from 'App/modules/EarSurgeriesListModule/MastoidectomyComponents/ModifiedRadical'
import React from 'react'

function ModifiedRadical() {
  return (
   <>
   <ModifiedRadicalModule />
   </>
  )
}

export default WithAuth(ModifiedRadical);

