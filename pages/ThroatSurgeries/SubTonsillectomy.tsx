
import WithAuth from 'App/HOC/withAuth';
import SubTonsillectomyModule from '../../App/modules/ThroatSurgeriesListModule/TonsillectomyComponents/SubTonsillectomy'
import React from 'react'

function SubTonsillectomy() {
  return (
   <>
   <SubTonsillectomyModule />
   </>
  )
}

export default WithAuth(SubTonsillectomy);

