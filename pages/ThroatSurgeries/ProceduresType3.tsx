
import WithAuth from 'App/HOC/withAuth';
import ProceduresType3Module from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/ProceduresType3'
import React from 'react'

function ProceduresType3() {
  return (
   <>
   <ProceduresType3Module />
   </>
  )
}

export default WithAuth(ProceduresType3);

