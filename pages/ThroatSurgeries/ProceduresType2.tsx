
import WithAuth from 'App/HOC/withAuth';
import ProceduresType2Module from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/ProceduresType2'
import React from 'react'

function ProceduresType2() {
  return (
   <>
   <ProceduresType2Module />
   </>
  )
}

export default WithAuth(ProceduresType2);

