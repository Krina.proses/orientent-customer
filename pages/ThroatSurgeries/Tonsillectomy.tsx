
import WithAuth from 'App/HOC/withAuth';
import TonsillectomyModule from '../../App/modules/ThroatSurgeriesListModule/TonsillectomyComponents/Tonsillectomy'
import React from 'react'

function Tonsillectomy() {
  return (
   <>
   <TonsillectomyModule />
   </>
  )
}

export default WithAuth(Tonsillectomy);

