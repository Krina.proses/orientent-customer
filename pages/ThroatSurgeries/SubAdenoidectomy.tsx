
import WithAuth from 'App/HOC/withAuth';
import SubAdenoidectomyModule from '../../App/modules/ThroatSurgeriesListModule/AdenoidectomyComponents/SubAdenoidectomy'
import React from 'react'

function SubAdenoidectomy() {
  return (
   <>
   <SubAdenoidectomyModule />
   </>
  )
}

export default WithAuth(SubAdenoidectomy);

