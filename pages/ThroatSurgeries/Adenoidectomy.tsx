
import WithAuth from 'App/HOC/withAuth';
import AdenoidectomyModule from '../../App/modules/ThroatSurgeriesListModule/AdenoidectomyComponents/Adenoidectomy'
import React from 'react'

function Adenoidectomy() {
  return (
   <>
   <AdenoidectomyModule />
   </>
  )
}

export default WithAuth(Adenoidectomy);

