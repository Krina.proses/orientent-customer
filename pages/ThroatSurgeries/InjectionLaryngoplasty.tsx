
import WithAuth from 'App/HOC/withAuth';
import InjectionLaryngoplastyModule from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/InjectionLaryngoplasty'
import React from 'react'

function InjectionLaryngoplasty() {
  return (
   <>
   <InjectionLaryngoplastyModule />
   </>
  )
}

export default WithAuth(InjectionLaryngoplasty);

