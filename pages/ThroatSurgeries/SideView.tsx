
import WithAuth from 'App/HOC/withAuth';
import NoseAnatomyModule from '../../App/modules/ThroatSurgeriesListModule/ThroatAnatomyComponents/SideView'
import React from 'react'

function SideView() {
  return (
   <>
   <NoseAnatomyModule />
   </>
  )
}

export default WithAuth(SideView);

