
import WithAuth from 'App/HOC/withAuth';
import PhonosurgeryModule from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/Phonosurgery'
import React from 'react'

function Phonosurgery() {
  return (
   <>
   <PhonosurgeryModule />
   </>
  )
}

export default WithAuth(Phonosurgery);

