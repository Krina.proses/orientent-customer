
import WithAuth from 'App/HOC/withAuth';
import PostTonsillectomyModule from '../../App/modules/ThroatSurgeriesListModule/TonsillectomyComponents/PostTonsillectomy'
import React from 'react'

function PostTonsillectomy() {
  return (
   <>
   <PostTonsillectomyModule />
   </>
  )
}

export default WithAuth(PostTonsillectomy);

