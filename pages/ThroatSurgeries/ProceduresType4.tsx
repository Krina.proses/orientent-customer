
import WithAuth from 'App/HOC/withAuth';
import ProceduresType4Module from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/ProceduresType4'
import React from 'react'

function ProceduresType4() {
  return (
   <>
   <ProceduresType4Module />
   </>
  )
}

export default WithAuth(ProceduresType4);

