
import WithAuth from 'App/HOC/withAuth';
import ThyroplastyModule from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/Thyroplasty'
import React from 'react'

function Thyroplasty() {
  return (
   <>
   <ThyroplastyModule />
   </>
  )
}

export default WithAuth(Thyroplasty);

