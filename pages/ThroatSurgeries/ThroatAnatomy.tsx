
import WithAuth from 'App/HOC/withAuth';
import ThroatAnatomyModule from '../../App/modules/ThroatSurgeriesListModule/ThroatAnatomyComponents/ThroatAnatomy'
import React from 'react'

function ThroatAnatomy() {
  return (
   <>
   <ThroatAnatomyModule />
   </>
  )
}

export default WithAuth(ThroatAnatomy);

