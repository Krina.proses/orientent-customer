
import WithAuth from 'App/HOC/withAuth';
import PhonomicrosurgeryModule from '../../App/modules/ThroatSurgeriesListModule/PhonosurgeryComponents/Phonomicrosurgery'
import React from 'react'

function Phonomicrosurgery() {
  return (
   <>
   <PhonomicrosurgeryModule />
   </>
  )
}

export default WithAuth(Phonomicrosurgery);

