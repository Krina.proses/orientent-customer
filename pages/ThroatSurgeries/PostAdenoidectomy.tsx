
import WithAuth from 'App/HOC/withAuth';
import PostAdenoidectomyModule from '../../App/modules/ThroatSurgeriesListModule/AdenoidectomyComponents/PostAdenoidectomy'
import React from 'react'

function PostAdenoidectomy() {
  return (
   <>
   <PostAdenoidectomyModule />
   </>
  )
}

export default WithAuth(PostAdenoidectomy);

