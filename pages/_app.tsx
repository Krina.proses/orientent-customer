import type { AppProps } from "next/app";
import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.scss";
import "nprogress/nprogress.css";
import {  ToastContainer } from "react-toastify";
import { UiProvider } from "App/contexts/ui/ui.provider";
import { AuthProvider } from "App/contexts/auth/auth.provider";
import NProgress from 'nprogress';
import { useRouter } from "next/router";
import { useEffect } from "react";

NProgress.configure({ showSpinner: false, minimum: 0.3 });

function MyApp({ Component, pageProps }: AppProps) {

  const router = useRouter()

  useEffect(() => {

    router.events.on('routeChangeStart', () => {
      NProgress.start();
    });

    router.events.on('routeChangeError', () => {
      NProgress.done();
    })

    router.events.on('routeChangeComplete', () => {
      NProgress.done();
    })

    return () => {

      router.events.off('routeChangeStart', () => {
      });

      router.events.off('routeChangeError', () => {
      })

      router.events.off('routeChangeComplete', () => {
      })
    }
  }, [])

  return (
    <UiProvider>
      <AuthProvider>
          <Component {...pageProps} />
          <ToastContainer
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </AuthProvider>
    </UiProvider>
  );
}

export default MyApp;
