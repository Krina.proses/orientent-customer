
import React from 'react'
import ResourcesModule from 'App/modules/ResourcesModule/Resources'
import WithAuth from 'App/HOC/withAuth'
const Resources = () => {
  return (
    <ResourcesModule/>
  )
}

export default WithAuth(Resources)