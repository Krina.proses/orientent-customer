import { NextPage } from "next";
import React from "react";
import LoginModule from "App/modules/LoginModule/Login";

const Login: NextPage = () => {
  return <LoginModule />;
};

export default Login;
