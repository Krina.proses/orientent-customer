import { NextPage } from "next";
import React from "react";
import HomeModule from "App/modules/HomeModule/Home";
import WithAuth from "App/HOC/withAuth";

const Home: NextPage = () => {
  return <HomeModule />;
};

export default WithAuth(Home);
