import { NextPage } from "next";
import React from "react";
import AppLinkModule from "App/modules/LoginModule/AppLink";

function AppLink() {
  return <AppLinkModule />;
}

export default AppLink;
