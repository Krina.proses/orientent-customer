
import WithAuth from 'App/HOC/withAuth';
import PolypectomyModule from '../../App/modules/NoseSurgeriesListModule/PolypectomyComponents/Polypectomy'
import React from 'react'

function Polypectomy() {
  return (
   <>
   <PolypectomyModule />
   </>
  )
}

export default WithAuth(Polypectomy);

