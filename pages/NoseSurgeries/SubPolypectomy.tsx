
import WithAuth from 'App/HOC/withAuth';
import SubPolypectomyModule from '../../App/modules/NoseSurgeriesListModule/PolypectomyComponents/SubPolypectomy'
import React from 'react'

function SubPolypectomy() {
  return (
   <>
   <SubPolypectomyModule />
   </>
  )
}

export default WithAuth(SubPolypectomy);

