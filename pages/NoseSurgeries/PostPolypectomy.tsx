
import WithAuth from 'App/HOC/withAuth';
import PostPolypectomyModule from '../../App/modules/NoseSurgeriesListModule/PolypectomyComponents/PostPolypectomy'
import React from 'react'

function PostPolypectomy() {
  return (
   <>
   <PostPolypectomyModule />
   </>
  )
}

export default WithAuth(PostPolypectomy);

