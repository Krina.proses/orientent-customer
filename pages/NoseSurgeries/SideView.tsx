
import WithAuth from 'App/HOC/withAuth';
import SideViewModule from '../../App/modules/NoseSurgeriesListModule/NoseAnatomyComponents/SideView'
import React from 'react'

function SideView() {
  return (
   <>
   <SideViewModule />
   </>
  )
}

export default WithAuth(SideView);

