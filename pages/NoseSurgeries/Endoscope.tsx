
import WithAuth from 'App/HOC/withAuth';
import EndoscopeModule from '../../App/modules/NoseSurgeriesListModule/FESSComponents/Endoscope'
import React from 'react'

function Endoscope() {
  return (
   <>
   <EndoscopeModule />
   </>
  )
}

export default WithAuth(Endoscope);

