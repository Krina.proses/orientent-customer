
import WithAuth from 'App/HOC/withAuth';
import PostSeptoplastyModule from '../../App/modules/NoseSurgeriesListModule/SeptoplastyComponents/PostSeptoplasty'
import React from 'react'

function PostSeptoplasty() {
  return (
   <>
   <PostSeptoplastyModule />
   </>
  )
}

export default WithAuth(PostSeptoplasty);

