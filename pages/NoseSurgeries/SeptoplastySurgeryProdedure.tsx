
import WithAuth from 'App/HOC/withAuth';
import SeptoplastySurgeryProdedureModule from '../../App/modules/NoseSurgeriesListModule/SeptoplastyComponents/SeptoplastySurgeryProdedure'
import React from 'react'

function SeptoplastySurgeryProdedure() {
  return (
   <>
   <SeptoplastySurgeryProdedureModule />
   </>
  )
}

export default WithAuth(SeptoplastySurgeryProdedure);

