
import WithAuth from 'App/HOC/withAuth';
import SeptoplastyModule from '../../App/modules/NoseSurgeriesListModule/SeptoplastyComponents/Septoplasty'
import React from 'react'

function Septoplasty() {
  return (
   <>
   <SeptoplastyModule />
   </>
  )
}

export default WithAuth(Septoplasty);

