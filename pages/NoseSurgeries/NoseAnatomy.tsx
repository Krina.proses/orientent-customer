
import WithAuth from 'App/HOC/withAuth';
import NoseAnatomyModule from '../../App/modules/NoseSurgeriesListModule/NoseAnatomyComponents/NoseAnatomy'
import React from 'react'

function NoseAnatomy() {
  return (
   <>
   <NoseAnatomyModule />
   </>
  )
}

export default WithAuth(NoseAnatomy);

