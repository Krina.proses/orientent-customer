
import WithAuth from 'App/HOC/withAuth';
import FESSModule from '../../App/modules/NoseSurgeriesListModule/FESSComponents/FESS'
import React from 'react'

function FESS() {
  return (
   <>
   <FESSModule />
   </>
  )
}

export default WithAuth(FESS);

