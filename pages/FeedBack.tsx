
import WithAuth from 'App/HOC/withAuth'
import FeedBackModule from 'App/modules/FeedBackModule/FeedBack'
import React from 'react'

const FeedBack = () => {
  return (
   <>
   <FeedBackModule/>
   </>
  )
}

export default WithAuth(FeedBack)

