/** @type {import('next').NextConfig} */
module.exports = {
    reactStrictMode: true,
    basePath: '',
    publicBasePath: '',
    trailingSlash: true,
    images: {
      domains: ['localhost', 'prosesindia.in'],
    },
  }